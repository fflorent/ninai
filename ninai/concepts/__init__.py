""" Holds, for the moment, the GeneralDemonym class only.
    (If more Concept subclasses become necessary, they will reside here as well.)
"""
from typing import Optional

from funcy import merge
from tfsl import Language, langs

import ninai.base.graph_client as graph
import ninai.base.utility as U
from ninai import Concept, Context, Framing, RendererInputs, RendererOutput, Scope, create_argument_filter
from ninai.base.concept import concept_argument_filter
from ninai.base.constructor import register_renderer, reregister_pre_hook

reregister_pre_hook("Concept", langs.mul_, "GeneralDemonym", langs.mul_)

@register_renderer("GeneralDemonym", langs.mul_)
def generaldemonym_render(inputs: RendererInputs) -> RendererOutput:
    """ Provides a Catena representing ``object_in`` in language ``language_in``.
    """
    _, framing, arguments, _ = inputs
    id_to_find = U.get_id_to_find(arguments["entity_id"].catena)

    language, _ = arguments["language"]
    out = graph.find_demonym(id_to_find, language, **merge({'type': 'demonym'}, framing.framing)) # TODO adjust kwargs
    return RendererOutput(out, Scope({}))

GeneralDemonym = create_argument_filter("GeneralDemonym", ["Concept"], [], argument_filter=concept_argument_filter)
