from funcy import merge
from tfsl import langs
from udiron.langs import ig, mul

import ninai.constructors as Con
import ninai.base.constants as C
import ninai.base.utility as U
from ninai import RendererInputs, RendererOutput, Scope
from ninai.base.constructor import register_renderer

@register_renderer("Speaker", langs.ig_)
def render_speaker_ig(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Speaker constructor in Igbo.
    """
    context, framing, arguments, scope = inputs
    output_catena = ig.generate_speaker(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("SpeakerListenerOthers", langs.ig_)
@register_renderer("SpeakerListener", langs.ig_)
@register_renderer("SpeakerOthers", langs.ig_)
def render_speaker_others_ig(inputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = ig.generate_speaker_plus(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Listener", langs.ig_)
def render_listener_ig(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = ig.generate_listener(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Action", langs.ig_)
def render_action_ig(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = arguments["predicate"].catena.expand()
    for thematic_rel, stmt in framing.get('thematic_relations').items():
        grammatical_feature_reqs = [claim.value.id for claim in stmt[C.requires_grammatical_feature]]
        syntactic_role = stmt[C.object_has_role][0].value
        # TODO: check for other qualifiers

        try:
            current_output = scope.scope['outputs'][thematic_rel][0]
        except (KeyError, IndexError):
            continue

        current_catena = current_output.catena
        current_catena.add_inflections(grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_catena = ig.add_nominal_subject(output_catena, current_catena)
        elif syntactic_role == C.direct_object:
            output_catena = ig.add_nominal_object(output_catena, current_catena)
    scope_outputs = scope.get('outputs', {})
    for output in scope_outputs:
        if output == "Locative":
            for entry in scope_outputs[output]:
                output_catena = ig.add_location_modifier(output_catena, entry.catena)
    output_catena = ig.inflect_verb(output_catena, **merge(scope.scope, framing.framing))
    if U.local_top_level(context):
        output_catena = mul.full_stop(output_catena)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Locative", langs.ig_)
def render_locative_ig(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Locative constructor in Igbo.
    """
    context, framing, arguments, scope = inputs
    object_catena = ig.mark_as_location(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))
