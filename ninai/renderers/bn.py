from typing import Tuple

import udiron.langs.bn as bn
import udiron.langs.mul as mul
from funcy import merge
from tfsl import Language, langs
from udiron import CatenaZipper

import ninai.base.constants as C
import ninai.base.utility as U
from ninai import Constructor, Context, RendererInputs, RendererOutput, Framing, Scope
from ninai.base.constructor import register_renderer, register_pre_hook, register_post_hook, constructor_subtype

def process_scope_outputs(base_catena: CatenaZipper, outputs: dict) -> CatenaZipper:
    """ Processes scope outputs, possibly attaching them to a base catena.
        Perhaps this should discard an output from the dict once it is attached to the catena?
    """
    for key in outputs:
        if constructor_subtype(key, "StativeLocation"):
            for entry in outputs[key]:
                base_catena = bn.add_location_modifier(base_catena, entry.catena)
    return base_catena

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.danda(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

@register_renderer("Identification", langs.bn_)
def render_identification_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Identification constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    object_catena = bn.add_copular_subject(arguments["object"].catena, arguments["subject"].catena)
    object_catena = process_scope_outputs(object_catena, scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["object"].scope)
    return RendererOutput(object_catena, output_scope)

@register_post_hook("Identification", langs.bn_)
def post_hook_identification_bn(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Identification constructor in Bengali.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Instance", langs.bn_)
def render_instance_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Instance constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    subject_catena = bn.add_noun_count(arguments["subject"].catena, **scope.scope)

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_renderer("Attribution", langs.bn_)
def render_attribution_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Attribution constructor in Bengali.
    """
    context, _, arguments, scope = inputs
    if U.local_top_level(context):
        attribute_catena = bn.add_copular_subject(arguments["attribute"].catena, arguments["subject"].catena, **scope.scope)
    else:
        # TODO: handle other modifiers here or just add them to scope handling?
        attribute_catena = bn.add_adjectival_modifier(arguments["subject"].catena, arguments["attribute"].catena)
    attribute_catena = process_scope_outputs(attribute_catena, scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["attribute"].scope)
    return RendererOutput(attribute_catena, output_scope)

@register_post_hook("Attribution", langs.bn_)
def post_hook_attribution_bn(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Attribution constructor in Bengali.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Superlative", langs.bn_)
def render_superlative_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Superlative constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    attribute_catena = bn.make_adjective_superlative(arguments["attribute"].catena)
    attribute_catena = process_scope_outputs(attribute_catena, scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["attribute"].scope)
    return RendererOutput(attribute_catena, output_scope)

@register_renderer("Locative", langs.bn_)
def render_locative_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Locative constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    object_catena = bn.mark_locative_case(arguments["object"].catena)
    return RendererOutput(object_catena, scope)

@register_renderer("Action", langs.bn_)
def render_action_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Action constructor in Bengali.
        At the moment the only qualifiers on 'has thematic relation' that are examined are
        'requires grammatical feature' and 'object has role', and even with the latter only
        the first value is taken.
        It also has yet to actually check that the predicate needs to be split before doing so.
    """
    _, framing, arguments, scope = inputs
    output_catena = arguments["predicate"].catena.expand()
    for thematic_rel, stmt in framing.get('thematic_relations').items():
        grammatical_feature_reqs = U.get_qualifier_values(stmt, C.requires_grammatical_feature)
        syntactic_role = stmt[C.object_has_role][0].value

        try:
            current_output = scope.scope['outputs'][thematic_rel][0]
        except (KeyError, IndexError):
            continue

        current_catena = current_output.catena
        current_catena = bn.add_thematic_inflections(current_catena, grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_catena = bn.add_nominal_subject(output_catena, current_catena)
            output_catena = bn.inflect_for_person(output_catena, current_catena)
        elif syntactic_role == C.direct_object:
            output_catena = bn.add_nominal_object(output_catena, current_catena)
    scope_outputs = scope.get('outputs', {})
    for output in scope_outputs:
        if output == "Supposition":
            for entry in scope_outputs[output]:
                subordinate_catena = mul.comma(entry.catena)
                output_catena = bn.add_subordinate_catena(output_catena, subordinate_catena)
    output_catena = bn.inflect_for_tense_aspect_mood(output_catena, **merge(scope.scope, framing.framing))
    return RendererOutput(output_catena, Scope({}))

@register_post_hook("Action", langs.bn_)
def post_hook_action_bn(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Action constructor in Bengali.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Speaker", langs.bn_)
def render_speaker_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Speaker constructor in Bengali.
    """
    _, _, _, scope = inputs
    output_catena = bn.generate_speaker(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Listener", langs.bn_)
def render_listener_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Speaker constructor in Bengali.
    """
    _, _, _, scope = inputs
    output_catena = bn.generate_listener(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("SpeakerListenerOthers", langs.bn_)
@register_renderer("SpeakerListener", langs.bn_)
@register_renderer("SpeakerOthers", langs.bn_)
def render_speaker_others_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for constructors involving the speaker and other parties in Bengali.
    """
    _, _, _, scope = inputs
    output_catena = bn.generate_speaker_plus(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Existence", langs.bn_)
def render_existence_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Existence constructor in Bengali.
    """
    _, framing, arguments, scope = inputs
    acha_catena = bn.note_existence(arguments["existent"].catena, **U.cat_scope_dicts(Scope(framing.framing), scope, arguments["existent"].scope).scope)
    acha_catena = process_scope_outputs(acha_catena, scope.get('outputs', {}))
    return RendererOutput(acha_catena, Scope({}))

@register_post_hook("Existence", langs.bn_)
def post_hook_existence_bn(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Existence constructor in Bengali.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Negation", langs.bn_)
def render_negation_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Negation constructor in Bengali.
    """
    _, framing, arguments, scope = inputs
    constructor_catena = bn.negate_catena(arguments["constructor"].catena)
    output_scope = U.cat_scope_dicts(scope, arguments["constructor"].scope)
    return RendererOutput(constructor_catena, output_scope)

@register_post_hook("Negation", langs.bn_)
def post_hook_negation_bn(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Negation constructor in Bengali.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Proximal", langs.bn_)
def render_proximal_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Proximal constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    output_catena = bn.attach_demonstrative_determiner(arguments["constructor"].catena, 'proximal', **scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("MedialDemonstrative", langs.bn_)
def render_medial_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Medial constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    output_catena = bn.attach_demonstrative_determiner(arguments["constructor"].catena, 'medial', **scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Distal", langs.bn_)
def render_distal_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Distal constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    output_catena = bn.attach_demonstrative_determiner(arguments["constructor"].catena, 'distal', **scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Greeting", langs.bn_)
def render_greeting_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Greeting constructor in Bengali.
    """
    context, _, arguments, scope = inputs
    output_catena = bn.generate_greeting(**scope.scope)
    if U.local_top_level(context):
        if C.emphasis in scope.scope:
            output_catena = mul.exclamation_mark(output_catena)
        else:
            output_catena = mul.danda(output_catena)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("ComparativeUnspecified", langs.bn_)
def render_comparativeunspecified_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the ComparativeUnspecified constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    output_catena = bn.intensify_adjective(arguments["attribute"].catena)
    return RendererOutput(output_catena, Scope({}))

@register_pre_hook("Supposition", langs.bn_)
def pre_hook_supposition_bn(constructor: Constructor, language: Language, context: Context, framing: Framing) -> Tuple[Constructor, Framing]:
    """ Hook run before rendering the Supposition constructor in Bengali.
    """
    thematic_relations_old = [framing.get("thematic_relations", "")]
    if not "thematic_relations_old" in framing.framing:
        framing = framing.set("thematic_relations_old", thematic_relations_old)
    else:
        framing = framing.set("thematic_relations_old", framing.get("thematic_relations_old") + thematic_relations_old)
    return constructor, framing

@register_post_hook("Supposition", langs.bn_)
def post_hook_supposition_bn(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Supposition constructor in Bengali.
    """
    thematic_relations_old = framing.get("thematic_relations_old").pop()
    framing = framing.set("thematic_relations", thematic_relations_old)
    return renderer_outputs, framing

@register_renderer("Supposition", langs.bn_)
def render_supposition_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Supposition constructor in Bengali.
    """
    _, _, arguments, scope = inputs
    output_catena = bn.make_condition(arguments["condition"].catena)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Possession", langs.bn_)
def render_possession_bn(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Possession constructor in Bengali.
    """
    context, _, arguments, scope = inputs
    if not U.local_top_level(context):
        output_catena = bn.make_possessive_construction(arguments["possessor"].catena, arguments["possessed"].catena)
    return RendererOutput(output_catena, Scope({}))
