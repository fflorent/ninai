""" Holds all renderers for Breton.
"""
from typing import Tuple

from tfsl import Language, langs
from udiron.langs import br, mul

import ninai.base.utility as U
from ninai import RendererInputs, RendererOutput
from ninai.base.constructor import register_renderer, register_post_hook
from ninai.base.context import Context
from ninai.base.framing import Framing

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.full_stop(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

@register_renderer("Instance", langs.br_)
def render_instance_br(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Instance constructor in Breton.
    """
    _, _, arguments, scope = inputs
    subject_catena = br.add_noun_count(arguments["subject"].catena, **scope.scope)

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_renderer("Identification", langs.br_)
def render_identification_br(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Identification constructor in Breton.
    """
    _, _, arguments, scope = inputs
    object_catena = br.add_copular_subject(arguments["object"].catena, arguments["subject"].catena, **scope.scope)

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["object"].scope)
    return RendererOutput(object_catena, output_scope)

@register_post_hook("Identification", langs.br_)
def post_hook_identification_br(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Identification constructor in Breton.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing
