from typing import Tuple

import udiron.langs.dag as dag
import udiron.langs.mul as mul
from tfsl import Language, langs

import ninai.base.constants as C
import ninai.base.utility as U
from ninai import RendererInputs, RendererOutput, Context, Framing, Scope
from ninai.base.constructor import register_renderer, register_post_hook, constructor_subtype

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.full_stop(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

def process_scope_outputs(base_catena, outputs):
    # TODO: scrap certain outputs if added to Catena?
    for key in outputs:
        if constructor_subtype(key, "StativeLocation"):
            for entry in outputs[key]:
                base_catena = dag.add_location_modifier(base_catena, entry.catena)
    return base_catena

def verb_at_end_of_sentence_dag(catena_in):
    return len(catena_in.get_right_children()) == 0

def nyini_as_subject_dag(catena_in):
    return catena_in.has_inflections(C.singular, C.second_person)

@register_renderer("Speaker", langs.dag_)
def render_speaker_dag(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = dag.generate_speaker(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Listener", langs.dag_)
def render_listener_dag(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = dag.generate_listener(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Instance", langs.dag_)
def render_instance_dag(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    subject_catena = dag.add_noun_count(arguments["subject"].catena, **scope.scope)

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_renderer("Locative", langs.dag_)
def render_locative_dag(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    object_catena = dag.mark_location(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))

@register_renderer("Action", langs.dag_)
def render_action_dag(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = arguments["predicate"].catena.expand()
    for thematic_rel, stmt in framing.get('thematic_relations').items():
        grammatical_feature_reqs = [claim.value.id for claim in stmt[C.requires_grammatical_feature]]
        syntactic_role = stmt[C.object_has_role][0].value
        # TODO: check for other qualifiers

        try:
            current_output = scope.scope['outputs'][thematic_rel][0]
        except (KeyError, IndexError):
            continue

        current_catena = current_output.catena
        current_catena.add_inflections(*grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_catena = dag.add_nominal_subject(output_catena, current_catena)
            output_catena = dag.inflect_for_person(output_catena, current_catena)
        elif syntactic_role == C.direct_object:
            output_catena = dag.add_nominal_object(output_catena, current_catena)
    output_catena = process_scope_outputs(output_catena, scope.get('outputs', {}))
    if verb_at_end_of_sentence_dag(output_catena):
        if not nyini_as_subject_dag(output_catena):
            output_catena.add_inflections(C.final_dag)
    return RendererOutput(output_catena, Scope({}))

@register_post_hook("Action", langs.dag_)
def post_hook_action_dag(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Action constructor in Dagbani.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing
