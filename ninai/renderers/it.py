""" Renderers for Italian.
"""
from typing import Tuple

from funcy import merge
from tfsl import Language, langs
from udiron.langs import it, mul

import ninai.base.utility as U
from ninai import RendererInputs, RendererOutput
from ninai.base.constructor import register_renderer, register_post_hook
from ninai.base.context import Context
from ninai.base.framing import Framing
from ninai.base.graph_client import register_sense_refine

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.full_stop(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

@register_renderer("Identification", langs.it_)
def render_identification_it(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Identification constructor in Italian.
    """
    context, framing, arguments, scope = inputs
    object_catena = it.add_copular_subject(arguments["object"].catena, arguments["subject"].catena, **merge(framing.framing, scope.scope))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["object"].scope)
    return RendererOutput(object_catena, output_scope)

@register_renderer("Instance", langs.it_)
def render_instance_it(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Instance constructor in Italian.
    """
    context, framing, arguments, scope = inputs
    subject_catena = it.add_noun_count(arguments["subject"].catena, **merge(framing.framing, scope.scope))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_post_hook("Identification", langs.it_)
def post_hook_identification_it(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Identification constructor in Italian.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing
