""" Holds all renderers for Swedish.
"""

from collections import defaultdict
from typing import Tuple

from funcy import merge
from tfsl import Language, langs
from udiron import CatenaZipper
import udiron.langs.sv as sv
import udiron.langs.mul as mul

import ninai.base.constants as C
import ninai.base.utility as U
from ninai import Context, Framing, RendererInputs, RendererOutput, Scope
from ninai.base.constructor import register_renderer, register_post_hook, constructor_subtype

def process_scope_outputs(base_catena: CatenaZipper, outputs: dict) -> CatenaZipper:
    """ Processes scope outputs, possibly attaching them to a base catena.
        Perhaps this should discard an output from the dict once it is attached to the catena?
    """
    for key in outputs:
        if constructor_subtype(key, "StativeLocation"):
            for entry in outputs[key]:
                base_catena = sv.add_location_modifier(base_catena, entry.catena)
    return base_catena

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.full_stop(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

def propagate_scope_outputs(outputs):
    """ Packages scope outputs for upward propagation to the renderer of another constructor.
    """
    scope_outputs = defaultdict(list)
    for key in outputs:
        if constructor_subtype(key, "StativeLocation"):
            for entry in outputs[key]:
                scope_outputs[key].append(entry)
    return Scope({'outputs': dict(scope_outputs)})

@register_renderer("Identification", langs.sv_)
def render_identification_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Identification constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    object_catena = sv.add_copular_subject(arguments["object"].catena, arguments["subject"].catena)
    object_catena = process_scope_outputs(object_catena, scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["object"].scope)
    return RendererOutput(object_catena, output_scope)

@register_post_hook("Identification", langs.sv_)
def post_hook_identification_sv(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Identification constructor in Swedish.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Instance", langs.sv_)
def render_instance_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Instance constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    subject_catena = sv.add_noun_count(arguments["subject"].catena, **scope.scope)

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_renderer("Attribution", langs.sv_)
def render_attribution_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Attribution constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    attribute_catena = arguments["attribute"].catena
    if not attribute_catena.has_rel(C.adposition):
        attribute_catena = sv.inflect_for_gender(arguments["attribute"].catena, arguments["subject"].catena)
    if U.local_top_level(context):
        attribute_catena = sv.add_copular_subject(attribute_catena, arguments["subject"].catena)
    else:
        attribute_catena = sv.add_adjectival_modifier(arguments["subject"].catena, attribute_catena)
    attribute_catena = process_scope_outputs(attribute_catena, arguments["attribute"].scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["attribute"].scope)
    return RendererOutput(attribute_catena, output_scope)

@register_post_hook("Attribution", langs.sv_)
def post_hook_attribution_sv(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Attribution constructor in Swedish.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Superlative", langs.sv_)
def render_superlative_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Superlative constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    attribute_catena = sv.make_adjective_superlative(arguments["attribute"].catena)

    output_scope = propagate_scope_outputs(scope.get('outputs', {}))
    return RendererOutput(attribute_catena, output_scope)

@register_renderer("Locative", langs.sv_)
def render_locative_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Locative constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    object_catena = sv.mark_as_location(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))

@register_renderer("Existence", langs.sv_)
def render_existence_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Existence constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    existence_catena = sv.build_existence_catena(arguments["existent"].catena, **U.cat_scope_dicts(scope, arguments["existent"].scope).scope)
    return RendererOutput(existence_catena, Scope({}))

@register_post_hook("Existence", langs.sv_)
def post_hook_existence_sv(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Existence constructor in Swedish.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Negation", langs.sv_)
def render_negation_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Negation constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    negated_catena = sv.negate_catena(arguments["constructor"].catena)
    return RendererOutput(negated_catena, Scope({}))

@register_post_hook("Negation", langs.sv_)
def post_hook_negation_sv(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Negation constructor in Swedish.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Action", langs.sv_)
def render_action_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Action constructor in French.
        At the moment the only qualifiers on 'has thematic relation' that are examined are
        'requires grammatical feature' and 'object has role', and even with the latter only
        the first value is taken.
        It also has yet to actually check that the predicate needs to be split before doing so.
    """
    _, framing, arguments, scope = inputs
    output_catena = arguments["predicate"].catena.expand()
    for thematic_rel, stmt in framing.get('thematic_relations').items():
        grammatical_feature_reqs = U.get_qualifier_values(stmt, C.requires_grammatical_feature)
        syntactic_role = stmt[C.object_has_role][0].value

        try:
            current_output = scope.scope['outputs'][thematic_rel][0]
        except (KeyError, IndexError):
            continue

        current_catena = current_output.catena
        current_catena = sv.add_thematic_inflections(current_catena, grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_catena = sv.add_nominal_subject(output_catena, current_catena)
        elif syntactic_role == C.direct_object:
            output_catena = sv.add_nominal_object(output_catena, current_catena)
    output_catena = sv.inflect_for_tense(output_catena, **merge(scope.scope, framing.framing))
    scope_outputs = scope.get('outputs', {})
    for output in scope_outputs:
        if constructor_subtype(output, "GeneralLocation"):
            for entry in scope_outputs[output]:
                output_catena = sv.add_location_modifier(output_catena, entry.catena)
    return RendererOutput(output_catena, Scope({}))

@register_post_hook("Action", langs.sv_)
def post_hook_action_sv(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Action constructor in Swedish.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Speaker", langs.sv_)
def render_speaker_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Speaker constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    output_catena = sv.generate_speaker(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("SpeakerListenerOthers", langs.sv_)
@register_renderer("SpeakerListener", langs.sv_)
@register_renderer("SpeakerOthers", langs.sv_)
def render_speaker_others_sv(inputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    output_catena = sv.generate_speaker_plus(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Listener", langs.sv_)
def render_listener_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Listener constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    output_catena = sv.generate_listener(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Greeting", langs.sv_)
def render_greeting_sv(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Greeting constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    output_catena = sv.generate_greeting(**scope.scope)
    if U.local_top_level(context):
        if C.emphasis in scope.scope:
            output_catena = mul.exclamation_mark(output_catena)
        else:
            output_catena = mul.full_stop(output_catena)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Adessive", langs.sv_)
@register_renderer("Proxillative", langs.sv_)
def render_near(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Adessive constructor in Swedish.
    """
    context, framing, arguments, scope = inputs
    object_catena = sv.mark_near(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))
