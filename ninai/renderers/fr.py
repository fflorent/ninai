""" Holds all renderers for French.
"""
from typing import Tuple

from funcy import merge
from tfsl import Language, langs
from udiron import CatenaZipper
from udiron.langs import fr, mul

import ninai.base.constants as C
import ninai.base.utility as U
from ninai import RendererInputs, RendererOutput, Scope
from ninai.base.constructor import register_renderer, register_post_hook, constructor_subtype
from ninai.base.context import Context
from ninai.base.framing import Framing

def process_scope_outputs(base_catena: CatenaZipper, outputs: dict) -> CatenaZipper:
    """ Processes scope outputs, possibly attaching them to a base catena.
        Perhaps this should discard an output from the dict once it is attached to the catena?
    """
    for key in outputs:
        if constructor_subtype(key, "StativeLocation"):
            for entry in outputs[key]:
                base_catena = fr.add_location_modifier(base_catena, entry.catena)
    return base_catena

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.full_stop(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

@register_renderer("Action", langs.fr_)
def render_action_fr(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Action constructor in French.
        At the moment the only qualifiers on 'has thematic relation' that are examined are
        'requires grammatical feature' and 'object has role', and even with the latter only
        the first value is taken.
        It also has yet to actually check that the predicate needs to be split before doing so.
    """
    context, framing, arguments, scope = inputs
    output_catena = arguments["predicate"].catena.expand()
    for thematic_rel, stmt in framing.get('thematic_relations').items():
        grammatical_feature_reqs = U.get_qualifier_values(stmt, C.requires_grammatical_feature)
        syntactic_role = stmt[C.object_has_role][0].value

        try:
            current_output = scope.scope['outputs'][thematic_rel][0]
        except (KeyError, IndexError):
            continue

        current_catena = current_output.catena
        current_catena.add_inflections(*grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_catena = fr.add_nominal_subject(output_catena, current_catena)
            output_catena = fr.inflect_for_person(output_catena, current_catena)
            output_catena = fr.inflect_for_number(output_catena, current_catena)
        elif syntactic_role == C.direct_object:
            output_catena = fr.add_nominal_object(output_catena, current_catena)
    output_catena = fr.inflect_for_tense(output_catena, **merge(scope.scope, framing.framing))
    return RendererOutput(output_catena, Scope({}))

@register_post_hook("Action", langs.fr_)
def post_hook_action_fr(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Action constructor in French.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Speaker", langs.fr_)
def render_speaker_fr(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Speaker constructor in French.
    """
    context, framing, arguments, scope = inputs
    output_catena = fr.generate_speaker(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("SpeakerListenerOthers", langs.fr_)
@register_renderer("SpeakerOthers", langs.fr_)
@register_renderer("SpeakerListener", langs.fr_)
def render_speaker_others_fr(inputs) -> RendererOutput:
    """ Renderer for a number of constructors representing the speaker and others in French.
    """
    context, framing, arguments, scope = inputs
    output_catena = fr.generate_speaker_plus(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Listener", langs.fr_)
def render_listener_fr(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Listener constructor in French.
    """
    context, framing, arguments, scope = inputs
    output_catena = fr.generate_listener(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Locative", langs.fr_)
def render_locative_fr(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Locative constructor in French.
    """
    context, framing, arguments, scope = inputs
    object_catena = fr.mark_as_location(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))

@register_renderer("Instance", langs.fr_)
def render_instance_fr(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Instance constructor in French.
    """
    context, framing, arguments, scope = inputs
    subject_catena = fr.add_noun_count(arguments["subject"].catena, **scope.scope)

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_renderer("Identification", langs.fr_)
def render_identification_fr(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Identification constructor in French.
    """
    context, framing, arguments, scope = inputs
    subject_catena = fr.add_noun_count_definite(arguments["subject"].catena)
    object_catena = fr.add_copular_subject(arguments["object"].catena, subject_catena)
    object_catena = process_scope_outputs(object_catena, scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["object"].scope)
    return RendererOutput(object_catena, output_scope)

@register_post_hook("Identification", langs.fr_)
def post_hook_identification_fr(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Identification constructor in Swedish.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing
