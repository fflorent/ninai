""" Holds all renderers for German.
"""
from typing import Tuple

from funcy import merge
from tfsl import Language, langs
from udiron import CatenaZipper
from udiron.langs import de, mul

import ninai.base.constants as C
import ninai.base.utility as U
import ninai.constructors as Con
from ninai import Concept, Constructor, Context, Framing, RendererInputs, RendererOutput, Scope
from ninai.base.constructor import register_renderer, register_post_hook, register_argument_pre_hook, register_argument_post_hook, constructor_subtype
from ninai.base.refcontext import Backref
from ninai.base.graph_client import register_sense_refine
from ninai.concepts import GeneralDemonym

def process_scope_outputs(base_catena: CatenaZipper, outputs: dict) -> CatenaZipper:
    """ Processes scope outputs, possibly attaching them to a base catena.
        Perhaps this should discard an output from the dict once it is attached to the catena?
    """
    for key in outputs:
        if constructor_subtype(key, "StativeLocation"):
            for entry in outputs[key]:
                base_catena = de.add_location_modifier(base_catena, entry.catena)
    return base_catena

def add_full_stop_if_needed(renderer_outputs: RendererOutput, context: Context) -> RendererOutput:
    """ Adds a full stop to a catena if the catena is the output from rendering a top-level constructor.
    """
    if U.local_top_level(context):
        renderer_outputs = RendererOutput(mul.full_stop(renderer_outputs.catena), renderer_outputs.scope)
    return renderer_outputs

@register_renderer("Identification", langs.de_)
def render_identification_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Identification constructor in German.
    """
    context, framing, arguments, scope = inputs
    object_catena = de.inflect_for_case(arguments["object"].catena, C.nominative)
    # TODO: find a better way to handle framing/scope merges
    object_catena = de.add_copular_subject(object_catena, arguments["subject"].catena, **merge(framing.framing, scope.scope))
    object_catena = process_scope_outputs(object_catena, scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["object"].scope)
    return RendererOutput(object_catena, output_scope)

@register_post_hook("Identification", langs.de_)
def post_hook_identification_de(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Identification constructor in German.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_argument_pre_hook("Identification", langs.de_, 'subject')
def pre_hook_identification_subject_de(subject: Constructor, language: Language, context: Context, framing: Framing) -> Tuple[Constructor, Framing]:
    """ Hook run before rendering the subject argument of the Attribution constructor in German.
    """
    target_item = None
    constructor_type, identifier, _, arguments, _ = subject
    if constructor_type == "Concept":
        target_item = arguments["entity_id"]
    elif constructor_type == "Backref":
        target_item = framing.get(C.FRAMING_CONTEXT)[identifier]

    if target_item:
        gender_stmts = U.get_gender_stmts(target_item)
        if len(gender_stmts) != 0:
            gender_value = gender_stmts[0].value
            if gender_value in [C.male, C.cis_male, C.trans_male]:
                framing = framing.set('subject_gender', 'masc')
            elif gender_value in [C.female, C.cis_female, C.trans_female]:
                framing = framing.set('subject_gender', 'fem')
    return subject, framing

@register_argument_post_hook("Attribution", langs.de_, 'subject')
def post_hook_attribution_subject_de(subject: RendererOutput, language: Language, context: Context, framing: Framing) -> Framing:
    """ Hook run after rendering the subject argument of the Attribution constructor in German.
    """
    if subject.catena.get_config().get('subject_gender', False):
        framing = framing.delete('subject_gender')
    return framing

@register_argument_pre_hook("Attribution", langs.de_, 'attribute')
def pre_hook_attribution_attribute_de(attribute: Constructor, language: Language, context: Context, framing: Framing) -> Tuple[Constructor, Framing]:
    """ Hook run before rendering the attribute argument of the Attribution constructor in German.
    """
    constructor_type, _, _, _, _ = attribute
    if constructor_type == "GeneralDemonym":
        framing = framing.set('pos_filter', C.adjective)
    return attribute, framing

@register_argument_post_hook("Attribution", langs.de_, 'attribute')
def post_hook_attribution_attribute_de(attribute: RendererOutput, language: Language, context: Context, framing: Framing) -> Framing:
    """ Hook run after rendering the attribute argument of the Attribution constructor in German.
    """
    if attribute.catena.get_config().get('pos_filter', False):
        framing = framing.delete('pos_filter')
    return framing

@register_renderer("Attribution", langs.de_)
def render_attribution_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Attribution constructor in German.
    """
    context, framing, arguments, scope = inputs
    if U.local_top_level(context):
        attribute_catena = de.inflect_for_gender(arguments["attribute"].catena, arguments["subject"].catena)
        attribute_catena = de.add_copular_subject(attribute_catena, arguments["subject"].catena)
    elif constructor_subtype(U.originating_constructor(arguments["attribute"].catena), "StativeLocation"):
        attribute_catena = de.add_location_modifier(arguments["subject"].catena, arguments["attribute"].catena)
    else:
        attribute_catena = de.inflect_for_gender(arguments["attribute"].catena, arguments["subject"].catena)
        attribute_catena = de.add_adjectival_modifier(arguments["subject"].catena, attribute_catena)
    attribute_catena = process_scope_outputs(attribute_catena, arguments["attribute"].scope.get('outputs', {}))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope, arguments["attribute"].scope)
    return RendererOutput(attribute_catena, output_scope)

@register_post_hook("Attribution", langs.de_)
def post_hook_attribution_de(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Attribution constructor in German.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Instance", langs.de_)
def render_instance_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Instance constructor in German.
    """
    context, framing, arguments, scope = inputs
    subject_catena = de.add_noun_count(arguments["subject"].catena, **merge(framing.framing, scope.scope))

    output_scope = U.cat_scope_dicts(scope, arguments["subject"].scope)
    return RendererOutput(subject_catena, output_scope)

@register_renderer("Locative", langs.de_)
def render_locative_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Locative constructor in German.
    """
    context, framing, arguments, scope = inputs
    object_catena = de.mark_as_location(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))

@register_sense_refine(langs.de_)
def sense_refine_de(candidate_paths, new_config, **scope):
    """ Used to filter a list of candidate senses when rendering a Concept in German.
    """
    desired_pos = None
    if 'pos_filter' in scope:
        desired_pos = scope['pos_filter']
        new_config = new_config.set('pos_filter', desired_pos)
        candidate_paths = {k: v for (k, v) in candidate_paths.items() if U.is_desired_pos(k, desired_pos)}

    if 'subject_gender' in scope:
        desired_gender = C.feminine if scope['subject_gender'] == 'fem' else C.masculine
        candidate_paths = {k: v for (k, v) in candidate_paths.items() if U.is_desired_gender(k, desired_gender)}
        new_config = new_config.set('subject_gender', desired_gender)

    return candidate_paths, new_config

@register_renderer("Action", langs.de_)
def render_action_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Action constructor in German.
        At the moment the only qualifiers on 'has thematic relation' that are examined are
        'requires grammatical feature' and 'object has role', and even with the latter only
        the first value is taken.
    """
    _, framing, arguments, scope = inputs
    output_catena = arguments["predicate"].catena
    output_catena = de.pop_if_needed(output_catena)
    for thematic_rel, stmt in framing.get('thematic_relations').items():
        if thematic_rel == 'None':
            output_catena = de.add_empty_subject(output_catena)
            continue
        grammatical_feature_reqs = U.get_qualifier_values(stmt, C.requires_grammatical_feature)
        syntactic_role = stmt[C.object_has_role][0].value

        try:
            current_output = scope.scope['outputs'][thematic_rel][0]
        except (KeyError, IndexError):
            continue

        current_catena = current_output.catena
        current_catena.add_inflections(*grammatical_feature_reqs)
        if syntactic_role == C.subject:
            output_catena = de.add_nominal_subject(output_catena, current_catena)
            output_catena = de.inflect_for_number(output_catena, current_catena)
            output_catena = de.inflect_for_person(output_catena, current_catena)
        elif syntactic_role == C.direct_object:
            output_catena = de.add_nominal_object(output_catena, current_catena)
        elif syntactic_role == C.indirect_object:
            output_catena = de.add_indirect_object(output_catena, current_catena)
    output_catena = de.inflect_for_tense(output_catena, **merge(scope.scope, framing.framing))
    scope_outputs = scope.get('outputs', {})
    for output in scope_outputs:
        if constructor_subtype(output, "GeneralLocation"):
            for entry in scope_outputs[output]:
                output_catena = de.add_location_modifier(output_catena, entry.catena)
    return RendererOutput(output_catena, Scope({}))

@register_post_hook("Action", langs.de_)
def post_hook_action_de(renderer_outputs: RendererOutput, language: Language, context: Context, framing: Framing) -> Tuple[RendererOutput, Framing]:
    """ Hook run after rendering the Action constructor in German.
    """
    return add_full_stop_if_needed(renderer_outputs, context), framing

@register_renderer("Speaker", langs.de_)
def render_speaker_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Speaker constructor in German.
    """
    context, framing, arguments, scope = inputs
    output_catena = de.generate_speaker(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("SpeakerListenerOthers", langs.de_)
@register_renderer("SpeakerOthers", langs.de_)
@register_renderer("SpeakerListener", langs.de_)
def render_speaker_others_de(inputs) -> RendererOutput:
    """ Renderer for a number of constructors representing the speaker and others in German.
    """
    context, framing, arguments, scope = inputs
    output_catena = de.generate_speaker_plus(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Listener", langs.de_)
def render_listener_de(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Listener constructor in German.
    """
    context, framing, arguments, scope = inputs
    output_catena = de.generate_listener(**scope.scope)
    return RendererOutput(output_catena, Scope({}))

@register_renderer("Adessive", langs.de_)
def render_adessive(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Adessive constructor in German.
    """
    context, framing, arguments, scope = inputs
    object_catena = de.mark_near_static(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))

@register_renderer("Proxillative", langs.de_)
def render_proxillative(inputs: RendererInputs) -> RendererOutput:
    """ Renderer of the Proxillative constructor in German.
    """
    context, framing, arguments, scope = inputs
    object_catena = de.mark_near_dynamic(arguments["object"].catena)
    return RendererOutput(object_catena, Scope({}))
