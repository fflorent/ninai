""" Loads all renderers.
    To load the renderers for a specific language, name that module directly:
    "import ninai.renderers.bn as bn" rather than "from ninai.renderers import bn"
"""

import ninai.renderers.bn as bn
import ninai.renderers.br as br
# import ninai.renderers.dag as dag (TODO: reinsert when grammar better understood)
import ninai.renderers.de as de
import ninai.renderers.fr as fr
import ninai.renderers.ig as ig
import ninai.renderers.it as it
import ninai.renderers.mul as mul
import ninai.renderers.sv as sv
