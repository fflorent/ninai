""" Holds renderers for various modifier constructors that may be applied to all languages.
"""
from typing import Tuple

from funcy import merge
from tfsl import Language, langs

import ninai.base.constants as C
import ninai.base.utility as U
from ninai import Constructor, Context, RendererInputs, RendererOutput, Framing, Scope
from ninai.base.constructor import register_renderer, register_pre_hook

def typical_framing_handling(constructor_type: str, scope_in: dict) -> None:
    """ Defines and registers methods for a constructor as follows:
        - On a rendering pre hook, the scope dict passed in is merged with the framing dict provided.
        - On rendering, if a scope output is present, then it is merely returned; otherwise the scope dict is returned as an output.
    """
    @register_renderer(constructor_type, langs.mul_)
    def render_stuff(inputs: RendererInputs) -> RendererOutput:
        _, _, _, scope = inputs
        if outputs := scope.get("outputs", False):
            return next(v[0] for (k, v) in outputs.items())
        return RendererOutput(None, Scope(scope_in))

    @register_pre_hook(constructor_type, langs.mul_)
    def pre_hook_stuff(constructor: Constructor, language: Language, context: Context, framing: Framing) -> Tuple[Constructor, Framing]:
        return constructor, Framing(merge(framing.framing, scope_in))

typical_framing_handling("Definite", {C.definite: True})

# # TODO: adjust the remainder of the following to use Qids
typical_framing_handling("PastTense", {C.tense: {'main': C.past}})
typical_framing_handling("PresentTense", {C.tense: {'main': C.present}})
typical_framing_handling("FutureTense", {C.tense: {'main': C.future}})
typical_framing_handling("RemotePast", {C.tense: {'main': C.past, 'refine': 'remote'}})
typical_framing_handling("RecentPast", {C.tense: {'main': C.past, 'refine': 'recent'}})
typical_framing_handling("Hesternal", {C.tense: {'main': C.past, 'refine': C.hesternal}})
typical_framing_handling("Hodiernal", {C.tense: {'main': C.present, 'refine': C.hodiernal}})
typical_framing_handling("Crastinal", {C.tense: {'main': C.future, 'refine': C.crastinal}})
typical_framing_handling("RecentFuture", {C.tense: {'main': C.future, 'refine': 'recent'}})
typical_framing_handling("RemoteFuture", {C.tense: {'main': C.future, 'refine': 'remote'}})

typical_framing_handling("Habitual", {C.aspect: C.habitual})
typical_framing_handling("Inchoative", {C.aspect: C.inchoative})
typical_framing_handling("Cessative", {C.aspect: C.cessative})
typical_framing_handling("Perfective", {C.aspect: C.perfective})
typical_framing_handling("Continuous", {C.aspect: C.continuous})
typical_framing_handling("Progressive", {C.aspect: C.progressive})

typical_framing_handling("Desiderative", {C.mood: C.desiderative})
typical_framing_handling("Jussive", {C.mood: C.jussive})
typical_framing_handling("Optative", {C.mood: C.optative})
typical_framing_handling("Imperative", {C.mood: C.imperative})
typical_framing_handling("Hortative", {C.mood: C.hortative})
typical_framing_handling("Dubitative", {C.mood: C.dubitative})
typical_framing_handling("Permissive", {C.mood: C.permissive})
typical_framing_handling("Renarrative", {C.mood: C.renarrative})

@register_renderer("Emphasis", langs.mul_)
def render_emphasis(inputs: RendererInputs) -> RendererOutput:
    """ Renderer for the Definite constructor in all languages.
        Not sure if this ought to be handled with typical_framing_handling as well.
    """
    return RendererOutput(None, Scope({C.emphasis: True}))

typical_framing_handling("Singular", {C.number: C.singular})
typical_framing_handling("Paucal", {C.number: C.paucal})
typical_framing_handling("Plural", {C.number: C.plural})

typical_framing_handling("Informal", {C.honorific: C.informal})
typical_framing_handling("Familiar", {C.honorific: C.familiar})
typical_framing_handling("Formal", {C.honorific: C.formal})
