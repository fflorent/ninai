""" Holds all constructors.
"""

from ninai.constructors.aspects import *
from ninai.constructors.discursives import *
from ninai.constructors.locatives import *
from ninai.constructors.modifiers import *
from ninai.constructors.moods import *
from ninai.constructors.nominals import *
from ninai.constructors.nonverbal_predicates import *
from ninai.constructors.subordinators import *
from ninai.constructors.tenses import *
from ninai.constructors.thematicrelations import *
