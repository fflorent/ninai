from ninai import create_argument_filter

GeneralAspect = create_argument_filter("GeneralAspect", [], [])
""" Base class for modifiers specifying the temporal extent of an action (akin to providing an aspect.)
"""

Habitual = create_argument_filter("Habitual", ["GeneralAspect"], [])
""" Used for events occurring ordinarily/usually/customarily.
"""

Inchoative = create_argument_filter("Inchoative", ["GeneralAspect"], [])
""" Used for events which are starting.
"""

Cessative = create_argument_filter("Cessative", ["GeneralAspect"], [])
""" Used for events which are ending.
"""

Perfective = create_argument_filter("Perfective", ["GeneralAspect"], [])
""" Used for events viewed as a whole.
"""

Continuous = create_argument_filter("Continuous", ["GeneralAspect"], [])
""" Used for events which are incomplete, viewed statically.
"""

Progressive = create_argument_filter("Progressive", ["GeneralAspect"], [])
""" Used for events which are incomplete, viewed dynamically.
"""
