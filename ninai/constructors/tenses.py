""" Holds constructors for different tenses.
"""

from ninai import create_argument_filter

GeneralTemporal = create_argument_filter("GeneralTemporal", [], [])
""" Base class for modifiers specifying the generic times of constructors
    (akin to providing a tense)."""

PastTense = create_argument_filter("PastTense", ["GeneralTemporal"], [])
""" Used for events occurring at any time before the current reference time::

    >>> Possession(Speaker(), Instance(Concept(Q(89))), PastTense()) # I had an apple.

    (See also general notes regarding GeneralTemporal.)
"""

Hesternal = create_argument_filter("Hesternal", ["PastTense"], [])
""" Used for events occurring on the day prior to the current reference time::

    >>> Possession(Speaker(), Instance(Concept(Q(89))), Hesternal()) # I had an apple yesterday.

    (See also general notes regarding GeneralTemporal.)
"""

RemotePast = create_argument_filter("RemotePast", ["PastTense"], [])
""" Used for events occurring at a time far removed in the past from the current reference time:

    >>> Possession(Speaker(), Instance(Concept(Q(89))), RemotePast()) # I had an apple a long time ago.

    (See also general notes regarding GeneralTemporal.)
"""

RecentPast = create_argument_filter("RecentPast", ["PastTense"], [])
""" Used for events occurring at a time close in the past to the current reference time:

    >>> Possession(Speaker(), Instance(Concept(Q(89))), RecentPast()) # I had an apple not long ago.

    (See also general notes regarding GeneralTemporal.)
"""

PresentTense = create_argument_filter("PresentTense", ["GeneralTemporal"], [])
""" Used for events occurring at or around the current reference time::

    >>> Possession(Speaker(), Instance(Concept(Q(165447))), PresentTense()) # I have a pen.

    (See also general notes regarding GeneralTemporal.)
"""

Hodiernal = create_argument_filter("Hodiernal", ["PresentTense"], [])
""" Used for events occurring on the same day as the current reference time::

    >>> Possession(Speaker(), Instance(Concept(Q(165447))), Hodiernal()) # I have a pen today.

    (See also general notes regarding GeneralTemporal.)
"""

FutureTense = create_argument_filter("FutureTense", ["GeneralTemporal"], [])
""" Used for events occurring at any time after the current reference time::

    >>> Possession(Speaker(), Instance(Concept(Q(10817602))), FutureTense()) # I will have a pineapple.

    (See also general notes regarding GeneralTemporal.)
"""

Crastinal = create_argument_filter("Crastinal", ["FutureTense"], [])
""" Used for events occurring on the day following the current reference time::

    >>> Possession(Speaker(), Instance(Concept(Q(10817602))), Crastinal()) # I will have a pineapple tomorrow.

    (See also general notes regarding GeneralTemporal.)
"""

RecentFuture = create_argument_filter("RecentFuture", ["FutureTense"], [])
""" Used for events occurring at a time close in the future to the current reference time:

    >>> Possession(Speaker(), Instance(Concept(Q(10817602))), RecentFuture()) # I will have a pineapple in a short time from now.

    (See also general notes regarding GeneralTemporal.)
"""

RemoteFuture = create_argument_filter("RemoteFuture", ["FutureTense"], [])
""" Used for events occurring at a time far removed in the future from the current reference time:

    >>> Possession(Speaker(), Instance(Concept(Q(10817602))), RemoteFuture()) # I will have a pineapple a long time from now.

    (See also general notes regarding GeneralTemporal.)
"""
