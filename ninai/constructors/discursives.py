from ninai import create_argument_filter

GeneralDiscursive = create_argument_filter("GeneralDiscursive", [], [])

Greeting = create_argument_filter("Greeting", ["GeneralDiscursive"], [])

ResponseGreeting = create_argument_filter("ResponseGreeting", ["GeneralDiscursive"], [])

Farewell = create_argument_filter("Farewell", ["GeneralDiscursive"], [])
