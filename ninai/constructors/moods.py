from ninai import create_argument_filter

GeneralMood = create_argument_filter("GeneralMood", [], [])
""" Base class for modifiers specifying the attitude of a speaker toward an action (akin to providing a mood).
"""

Desiderative = create_argument_filter("Desiderative", ["GeneralMood"], [])
""" Used for expressed desires of the subject to do something.
"""

Jussive = create_argument_filter("Jussive", ["GeneralMood"], [])
""" Used for indirect (i.e. non-second-person) exhortations.
"""

Optative = create_argument_filter("Optative", ["GeneralMood"], [])
""" Used for expressed desires for something to happen, without a suggestion to anyone that this be done.
"""

Imperative = create_argument_filter("Imperative", ["GeneralMood"], [])
""" Used for expressed desires to someone that something happen, demanding as much.
"""

Hortative = create_argument_filter("Hortative", ["GeneralMood"], [])
""" Used for expressed desires to someone that something happen, exhorting as much.
"""

Dubitative = create_argument_filter("Dubitative", ["GeneralMood"], [])
""" Used for events of whose truth the speaker is uncertain.
"""

Permissive = create_argument_filter("Permissive", ["GeneralMood"], [])
""" Used for events permitted by the speaker.
"""

Renarrative = create_argument_filter("Renarrative", ["GeneralMood"], [])
""" Used for events not witnessed by the speaker.
"""

