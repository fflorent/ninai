""" Holds constructors for specifying the locations of things
    and the locations from where or to where things are going.
"""

from ninai import create_argument_filter

GeneralLocation = create_argument_filter("GeneralLocation", [], ["object"])
""" Base class for arbitrary locations realized as adpositional
    phrases, whether or not in motion. """

StativeLocation = create_argument_filter("StativeLocation", ["GeneralLocation"], [])
""" Base class for the location of a constructor not in motion.
    These may be used, for example, as the second argument of Attribution."""

ActionLocation = create_argument_filter("ActionLocation", ["GeneralLocation"], [])
""" Base class for the location of a constructor in motion.
    These may be used, for example, as scope arguments of other constructors."""

Locative = create_argument_filter("Locative", ["StativeLocation"], [])
""" Used to indicate the generic location of a particular object (akin to 'in', 'at', 'on')::

    >>> Attribution(Concept(Q(319)), Location(Concept(Q(544)))) # Jupiter is in the solar system.

    (See also general notes regarding StativeLocation.)
"""

Adessive = create_argument_filter("Adessive", ["StativeLocation"], [])
""" Used to indicate that the containing constructor is near a particular object::

    >>> Attribution(Concept(Q(319)), Adessive(Concept(Q(544)))) # Jupiter is near the solar system.

    (See also general notes regarding StativeLocation.)
"""

Proxillative = create_argument_filter("Proxillative", ["ActionLocation"], [])
""" Used to indicate that the containing constructor is going to (near) a particular object::

    >>> Action("Q19279529", Concept(Q(319)), Proxillative(Concept(Q(544)))) # Jupiter is going to near the solar system.

    (See also general notes regarding ActionLocation.)
"""

Subessive = create_argument_filter("Subessive", ["StativeLocation"], [])
""" Used to indicate that the containing constructor is below a particular object::

    >>> Attribution(Concept(Q(319)), Subessive(Concept(Q(544)))) # Jupiter is below the solar system.

    (See also general notes regarding StativeLocation.)
"""
