from ninai import create_argument_filter

Speaker = create_argument_filter("Speaker", [], [])
""" Refers to the speaker:

    >>> Attribution(Speaker(), Concept(Q(59863338))) # I am large.

    Unlike Listener, this may not be pluralized; instead, see the nominal
    constructors below this one.
"""

SpeakerListener = create_argument_filter("SpeakerListener", [], [])
""" Refers to the speaker and the listener (but not others):

    >>> Attribution(SpeakerListener(), Concept(Q(59863338))) # We (you and I) are large.
"""

SpeakerListenerOthers = create_argument_filter("SpeakerListenerOthers", [], [])
""" Refers to the speaker, the listener, and others:

    >>> Attribution(SpeakerListenerOthers(), Concept(Q(59863338))) # We (you and I and others) are large.
"""

SpeakerOthers = create_argument_filter("SpeakerOthers", [], [])
""" Refers to the speaker and others (but not the listener):

    >>> Attribution(SpeakerOthers(), Concept(Q(59863338))) # We (but not you) are large.
"""

Listener = create_argument_filter("Listener", [], [])
""" Refers to a listener::

    >>> Attribution(Listener(), Concept(Q(59863338))) # You are large.

    May be pluralized, as Listener(Plural()), or formalized, as Listener(Formal()),
    or both, as Listener(Plural(), Formal()).
"""

Imperson = create_argument_filter("Imperson", [], [])
""" Used as an impersonal pronoun in situations that require it:

    >>> Attribution(Imperson(), Concept(Q(59863338))) # One is large.

    (This will likely be adjusted when renderers for it are created.)
"""
