from ninai import create_argument_filter

Supposition = create_argument_filter("Supposition", [], ['condition'])
""" Used to indicate a supposition under which a main Catena applies.
    This can be used for hypotheticals and preconditions depending
    on the mood of the condition argument.
"""
