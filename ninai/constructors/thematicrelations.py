""" Holds individual thematic relations.
"""

from tfsl import langs

import ninai.base.constants as C
from ninai import RendererInputs, RendererOutput
from ninai.base.constructor import register_renderer, create_argument_filter
from ninai.base.constructor import register_renderer

ThematicRelation = create_argument_filter("ThematicRelation", [], ["object"])

def thematicrelation_render(inputs: RendererInputs) -> RendererOutput:
    _, _, arguments, _ = inputs
    return RendererOutput(*arguments["object"])

Agent = create_argument_filter(C.agent, ["ThematicRelation"], [])
""" Denotes the participant in an action that causes or initiates the action:

    >>> # I give water to you.
    >>> Action(L(3230)["S1"], Agent(Speaker()), Patient(Concept("Q283")), Recipient(Listener()))

    Note that this does *not* always have to be a verb's subject.
"""

Patient = create_argument_filter(C.patient, ["ThematicRelation"], [])
""" Denotes the participant in an action upon which the action is carried out:

    >>> # I give water to you.
    >>> Action(L(3230)["S1"], Agent(Speaker()), Patient(Concept("Q283")), Recipient(Listener()))

    Sometimes synonymous with "theme". Note that this does *not* always have to be a verb's direct object.
"""

Experiencer = create_argument_filter(C.experiencer, ["ThematicRelation"], [])
""" Denotes the participant in an action that receives some sensory or emotional input:

    >>> # I heard the water.
    >>> Action("Q160289", Experiencer(Speaker()), Stimulus(Concept("Q283")), PastTense())
"""

Addressee = create_argument_filter(C.addressee, ["ThematicRelation"], [])
""" Denotes the participant in an action who is being addressed.

    (An example will be provided here once a verb is annotated with this relation.)
"""

Recipient = create_argument_filter(C.recipient, ["ThematicRelation"], [])
""" Denotes the participant in an action for whose benefit the action occurs:

    >>> # I give water to you.
    >>> Action(L(3230)["S1"], Agent(Speaker()), Patient(Concept("Q283")), Recipient(Listener()))

    Note that this does *not* always have to be a ditransitive verb's indirect object.
"""

Topic = create_argument_filter(C.topic, ["ThematicRelation"], [])
""" Denotes the participant in an action that undergoes the action without a state change:

    >>> # I gathered chestnuts.
    >>> Action(L(618346)["S1"], Agent(Speaker()), Topic(Instance(Concept("Q773987"), Plural())), PastTense())
"""

Source = create_argument_filter(C.source, ["ThematicRelation"], [])
""" Denotes where the action originates.

    (Unless provided as an explicit thematic relation on a sense, it may be possible to add sources
        just as scope arguments to the Action constructor.)
"""

Location = create_argument_filter(C.location, ["ThematicRelation"], [])
""" Denotes where the action occurs.

    (Unless provided as an explicit thematic relation on a sense, it may be possible to add locations
        just as scope arguments to the Action constructor.)
"""

Goal = create_argument_filter(C.goal, ["ThematicRelation"], [])
""" Denotes where the action terminates.

    (Unless provided as an explicit thematic relation on a sense, it may be possible to add goals
        just as scope arguments to the Action constructor.)
"""

Instrument = create_argument_filter(C.instrument, ["ThematicRelation"], [])
""" Denotes the participant in an action that is used to carry it out:

    >>> # I heard the water with the microphone.
    >>> Action("Q160289", Experiencer(Speaker()), Stimulus(Concept("Q283")), PastTense(), Instrument(Instance(Concept("Q46384"))))
"""

Stimulus = create_argument_filter(C.stimulus, ["ThematicRelation"], [])
""" Denotes the participant in an action that caused it (not necessarily voluntarily) in the first place.

    >>> # I heard the water.
    >>> Action("Q160289", Experiencer(Speaker()), Stimulus(Concept("Q283")), PastTense())
"""

thematic_relation_list = [C.agent, C.patient, C.experiencer, C.addressee, C.recipient,
                          C.topic, C.source, C.location, C.goal, C.instrument, C.stimulus]
for thematic_relation in thematic_relation_list:
    register_renderer(thematic_relation, langs.mul_)(thematicrelation_render)
