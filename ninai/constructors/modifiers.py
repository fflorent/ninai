from ninai import create_argument_filter

Instance = create_argument_filter("Instance", [], ['subject'])
""" Used to define a particular instance of a concept as an argument
    of a constructor, rather than the concept by itself::

    >>> Identification(Concept(Q(319)), Concept(Q(634))) # Jupiter is planet (the concept).
    >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.

    There may be instances, even in languages where definiteness is not marked,
    where omitting this constructor may not make a difference, but omitting it might
    yield undesirable or inconsistent results.
"""

Definite = create_argument_filter("Definite", [], [])
""" Used to mark an instance of a concept as definite::

    >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.
    >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)), Definite())) # Jupiter is the planet.

    There may be instances, even in languages where definiteness is not marked,
    where omitting this constructor may not make a difference, but omitting it might
    yield undesirable or inconsistent results.
"""

Comparative = create_argument_filter("Comparative", [], ['attribute', 'object'])
""" Used to specify that an attribute is more pronounced compared to
    some other object.

    >>> Attribution(Concept(Q(319)), Comparative(Concept(Q(59863338)), Concept(Q(193)))) # Jupiter is larger than Saturn.
"""

ComparativeUnspecified = create_argument_filter("ComparativeUnspecified", [], ['attribute'])
""" Used to specify that an attribute is more pronounced without specifying
    a standard for comparison:

    >>> Attribution(Concept(Q(319)), ComparativeUnspecified(Concept(Q(59863338)))) # Jupiter is larger.
"""

Superlative = create_argument_filter("Superlative", [], ['attribute'])
""" Used to specify that an attribute is the most pronounced::

    >>> Attribution(Concept(Q(319)), Superlative(Concept(Q(59863338)))) # Jupiter is the largest planet.

    As with other constructors, extra arguments may be added to clarify the superlative nature of the attribute.
"""

Negation = create_argument_filter("Negation", [], ['constructor'])
""" Used to negate different types of constructors::

    >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.
    >>> Negation(Identification(Concept(Q(319)), Instance(Concept(Q(634))))) # Jupiter is not a planet.

    Essentially an equivalent to the logical 'not' operator.
"""

Demonstrative = create_argument_filter("Demonstrative", [], ['constructor'])
""" Base class for demonstratives."""

Proximal = create_argument_filter("Proximal", ["Demonstrative"], [])
""" Used to specify that the contained constructor is closer to the speaker
    (much like 'kono' in Japanese)::

    >>> Attribution(Proximal(Concept(Q(10978))), Concept(Q(24245823))) # This grape is small.

    (See also general notes regarding Demonstrative.)
"""

MedialDemonstrative = create_argument_filter("MedialDemonstrative", ["Demonstrative"], [])
""" Used to specify that the contained constructor is closer to the listener
    (much like 'sono' in Japanese)::

    >>> Attribution(MedialDemonstrative(Concept(Q(10978))), Concept(Q(24245823))) # That grape is small.

    (See also general notes regarding Demonstrative.)
"""

Mesioproximal = MedialDemonstrative # to parallel Northern Sami's Mesiodistal

Distal = create_argument_filter("Distal", ["Demonstrative"], [])
""" Used to specify that the contained constructor is away from the speaker and the listener
    (much like 'ano' in Japanese)::

    >>> Attribution(Distal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

    (See also general notes regarding Demonstrative.)
"""

Mesiodistal = create_argument_filter("Mesiodistal", ["Demonstrative"], [])
""" Used to specify that the contained constructor is away from the speaker and the listener
    but still near (much like 'duot' in Northern Sami)::

    >>> Attribution(Mesiodistal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

    (See also general notes regarding Demonstrative.)
"""

FarDistal = create_argument_filter("FarDistal", ["Demonstrative"], [])
""" Used to specify that the contained constructor is away from the speaker and the listener
    and somewhat far (much like 'dot' in Northern Sami)::

    >>> Attribution(FarDistal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

    (See also general notes regarding Demonstrative.)
"""

Emphasis = create_argument_filter("Emphasis", [], [])
""" Metalinguistic emphasis, which might well occur anywhere and be rendered in many ways::

    >>> Identification(Distal(Concept(Q(575))), Concept(Q(3635662))) # সে রাত পূর্ণিমা।
    >>> Identification(Distal(Concept(Q(575)), Emphasis()), Concept(Q(3635662))) # সেই রাত পূর্ণিমা।

    (More examples to come when more requirements of emphasis handling arise in other languages.)
"""

Honorific = create_argument_filter("Honorific", [], [])
""" Base class for honorific specifiers."""

Informal = create_argument_filter("Informal", ["Honorific"], [])
""" Used to adjust the containing constructor to reflect an informal inflection
    (the level indicated by 'tu' in Hindustani)::

    >>> Identification(Listener(Informal()), Possession(Speaker(), Concept(Q(17297777)))) # Thou art my friend.

    (See also general notes regarding Honorific.)
"""

Familiar = create_argument_filter("Familiar", ["Honorific"], [])
""" Used to adjust the containing constructor to reflect a familiar inflection
    (the level indicated by 'tum' in Hindustani)::

    >>> Identification(Listener(Familiar()), Possession(Speaker(), Concept(Q(17297777)))) # You are my friend.

    (See also general notes regarding Honorific.)
"""

Formal = create_argument_filter("Formal", ["Honorific"], [])
""" Used to adjust the containing constructor to reflect a formal inflection
    (the level indicated by 'aap' in Hindustani)::

    >>> Identification(Listener(Formal()), Possession(Speaker(), Concept(Q(17297777)))) # You are my friend.

    (See also general notes regarding Honorific.)
"""

Reason = create_argument_filter("Reason", [], ['constructor'])
""" Used to indicate that the contained constructor is a cause of the
    statement that contains this modifier::

    >>> Existence(Listener(), Reason(Possession(Listener(), Concept(Q(9165))))) # You exist because you have a soul.

    The handling of extra arguments beyond the first is yet to be determined.
"""

Singular = create_argument_filter("Singular", [], [])
""" Pluralizes the constructor it modifies::

    >>> Possession(Speaker(), Plural(Concept(Q(133105)))) # I have legs.

    (This and other constructors without renderers are subject to abrupt change.)
"""

Plural = create_argument_filter("Plural", [], [])
""" Pluralizes the constructor it modifies::

    >>> Possession(Speaker(), Plural(Concept(Q(133105)))) # I have legs.

    (This and other constructors without renderers are subject to abrupt change.)
"""

Paucal = create_argument_filter("Paucal", [], [])
""" Paucalizes(?) the constructor it modifies::

    >>> Possession(Speaker(), Paucal(Concept(Q(133105)))) # I have a few legs.

    (This and other constructors without renderers are subject to abrupt change.)
"""
