
from typing import Callable, List, NamedTuple, Tuple, Union

from ninai.base.constructor import Constructor

class ArgumentLevel(NamedTuple):
    arg: Constructor
    arg_role: str

class ConstructorZipper(NamedTuple):
    zipper: List[ArgumentLevel]

def zip_constructor(arg: Constructor) -> ConstructorZipper:
    return ConstructorZipper([ArgumentLevel(arg, None)])

def constructorzipper_find(self: ConstructorZipper, target: str) -> ConstructorZipper:
    new_zipper = self.zipper.copy()
    cur_arg, _ = new_zipper[0]
    _, _, child_identifiers, _, _ = cur_arg
    path_to_target, _ = child_identifiers[target]
    for step in path_to_target:
        _, _, _, arguments, scope = cur_arg
        if 'scope_' in step:
            new_arg = next(filter(lambda arg: arg[0] == step[6:], scope))
        else:
            new_arg = arguments[step]
        new_zipper = [ArgumentLevel(new_arg, step)] + new_zipper
        cur_arg = new_arg
    return ConstructorZipper(new_zipper)
ConstructorZipper.find = constructorzipper_find

def constructorzipper_root(self: ConstructorZipper) -> ArgumentLevel:
    return self.zipper[-1]
ConstructorZipper.root = constructorzipper_root

def constructorzipper_empty(self: ConstructorZipper) -> bool:
    return not self.zipper
ConstructorZipper.empty = constructorzipper_empty

def constructorzipper_pop(self: ConstructorZipper) -> Tuple[ArgumentLevel, ConstructorZipper]:
    if not self.zipper:
        raise IndexError("ConstructorZipper is empty")
    return self.zipper[0], ConstructorZipper(self.zipper[1:])
ConstructorZipper.pop = constructorzipper_pop

def constructorzipper_wrap(self: ConstructorZipper, wrapping: Callable) -> ConstructorZipper:
    cur_arglevel, higher_levels = self.pop()
    cur_arg, cur_role = cur_arglevel
    new_arg = wrapping(cur_arg)
    new_arglevel = ArgumentLevel(new_arg, cur_role)
    adjusted_parent_list = adjust_parents(higher_levels, cur_arglevel, new_arglevel)
    return ConstructorZipper(adjusted_parent_list)
ConstructorZipper.wrap = constructorzipper_wrap

def adjust_parents(higher_parents: ConstructorZipper, original_level: ArgumentLevel, new_level: ArgumentLevel) -> list:
    """ Utility function that recursively modifies all higher entries in a given ConstructorZipper to reflect a change
        from the original_dependent to the new_dependent.
    """
    if higher_parents.empty():
        return [new_level]

    current_parent, higher_parents = constructorzipper_pop(higher_parents)

    current_arg, current_role = current_parent
    new_arg, new_role = new_level
    constructor_type, identifier, child_identifiers, core_arguments, scope_arguments = current_arg
    _, _, new_child_identifiers, _, _ = new_arg
    if 'scope_' in new_role:
        revised_role = new_role[6:]
        revised_core_arguments = core_arguments
        revised_scope_arguments = [arg for arg in scope_arguments if arg[0] != revised_role] + [new_arg]
    else:
        revised_core_arguments = core_arguments.copy()
        revised_core_arguments[new_role] = new_arg
        revised_scope_arguments = scope_arguments
    revised_child_identifiers = child_identifiers.copy()
    for (target, (path, instance)) in new_child_identifiers.items():
        revised_child_identifiers[target] = ((new_role,) + path, instance)
    revised_arg = Constructor(constructor_type, identifier, revised_child_identifiers, revised_core_arguments, revised_scope_arguments)

    revised_parent = ArgumentLevel(revised_arg, current_role)
    return [new_level] + adjust_parents(higher_parents, current_parent, revised_parent)
