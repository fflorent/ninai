""" Holds the Constructor class, container classes for renderer inputs/outputs,
    and the bulk of the logic for rendering Constructors.
"""

from collections import defaultdict
from typing import Any, Callable, List, NamedTuple, Optional, Tuple, Union

from tfsl import Language, langs
from udiron import CatenaConfig, CatenaZipper

from ninai.base.context import Context
from ninai.base.framing import Framing
from ninai.base.identifier import Identifier
from ninai.base.scope import Scope

class RendererInputs(NamedTuple):
    context: Context
    framing: Framing
    arguments: dict
    scope: Scope

class RendererOutput(NamedTuple):
    """ Container holding the output from, and scope information related to, a renderer.
    """
    catena: CatenaZipper
    scope: Scope

class CatenaTracking(NamedTuple):
    """ Container holding information about the constructor origin of a particular Catena.
    """
    parent_type: type #: The type of the parent of the constructor that yielded this Catena.
    parent_argument: str #: The argument position of the constructor that yielded this Catena.
    originator: type #: The type of the constructor that yielded this Catena.

class Constructor(NamedTuple):
    constructor_type: str
    identifier: Identifier
    child_identifiers: dict
    core_arguments: dict
    scope_arguments: list

def constructor_call(self: Constructor, language: Language) -> str:
    return str(render(self, language))
Constructor.__call__ = constructor_call

language_or_list_of_languages = Union[Language, List[Language]]

def merge_identifier_dicts(child_identifier_dict: dict, parent_identifier_dict: dict, role: str) -> dict:
    new_identifier_dict: dict = parent_identifier_dict.copy()
    for key, (path, child) in child_identifier_dict.items():
        new_identifier_dict[key] = ((role, ) + path, child)
    return new_identifier_dict

def handle_identifiers(arg: Constructor, role: str, child_identifiers: dict) -> dict:
    """ If the argument has an identifier, then it and the child identifiers known to
        the argument are saved.
    """
    new_child_identifiers: dict = child_identifiers.copy()
    if arg.identifier is not None:
        new_child_identifiers[arg.identifier] = ((role, ), type(arg))
    if isinstance(arg, Constructor):
        new_child_identifiers = merge_identifier_dicts(arg.child_identifiers, child_identifiers, role)
    return new_child_identifiers

def default_argument_filter(constructor_type: str, core_argument_list: List[str], *args) -> Constructor:
    """ Argument filter for constructors, assuming they were not declared with
        a custom argument filter.
    """
    identifier: str = ""
    scope_arguments: list = []
    core_arguments: dict = {}
    child_identifiers: dict = {}

    index: int = -1
    core_argument: str

    for index, core_argument in enumerate(core_argument_list):
        child_identifiers = handle_identifiers(args[index], core_argument, child_identifiers)
        core_arguments[core_argument] = args[index]
    for index, argument in enumerate(args[index+1:]):
        if isinstance(argument, Identifier):
            identifier = argument.identifier
            child_identifiers[identifier] = (tuple(), constructor_type)
        else:
            child_identifiers = handle_identifiers(argument, 'scope_' + argument.constructor_type, child_identifiers)
            scope_arguments.append(argument)
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments)

__constructor_renderers__ = defaultdict(dict)
def register_renderer(constructor_type: str,
                      language_or_languages: language_or_list_of_languages):
    """ Registers a renderer with a particular language or languages.
    """
    if isinstance(language_or_languages, Language):
        def register_method(method_in):
            __constructor_renderers__[constructor_type][language_or_languages] = method_in
            return method_in
    else:
        def register_method(method_in):
            for language in language_or_languages:
                __constructor_renderers__[constructor_type][language] = method_in
            return method_in
    return register_method

def reregister_renderer(original_type: str,
                        original_language: Language,
                        constructor_type: str,
                        language_or_languages: language_or_list_of_languages):
    desired_method = __constructor_renderers__[original_type][original_language]
    register_renderer(constructor_type, language_or_languages)(desired_method)

__constructor_arg_pre_hooks__ = defaultdict(lambda: defaultdict(dict))
def register_argument_pre_hook(constructor_type: str,
                                language_or_languages: language_or_list_of_languages,
                                argument: str):
    """ Registers a hook that is run before a particular argument to this constructor is rendered.
    """
    if isinstance(language_or_languages, Language):
        def set_arg_pre_hook(method_in):
            __constructor_arg_pre_hooks__[constructor_type][language_or_languages][argument] = method_in
    else:
        def set_arg_pre_hook(method_in):
            for language in language_or_languages:
                __constructor_arg_pre_hooks__[constructor_type][language][argument] = method_in
    return set_arg_pre_hook

__constructor_arg_post_hooks__ = defaultdict(lambda: defaultdict(dict))
def register_argument_post_hook(constructor_type: str,
                                language_or_languages: language_or_list_of_languages,
                                argument: str):
    """ Registers a hook that is run after a particular argument to this constructor is rendered.
    """
    if isinstance(language_or_languages, Language):
        def set_arg_post_hook(method_in):
            __constructor_arg_post_hooks__[constructor_type][language_or_languages][argument] = method_in
    else:
        def set_arg_post_hook(method_in):
            for language in language_or_languages:
                __constructor_arg_post_hooks__[constructor_type][language][argument] = method_in
    return set_arg_post_hook

# start pre-hook logic
__constructor_pre_hooks__ = defaultdict(dict)
def register_pre_hook(constructor_type: str,
                      language_or_languages: language_or_list_of_languages):
    """ Registers a hook that is run before rendering any arguments of this constructor.
    """
    if isinstance(language_or_languages, Language):
        def set_pre_hook(method_in):
            __constructor_pre_hooks__[constructor_type][language_or_languages] = method_in
    else:
        def set_pre_hook(method_in):
            for language in language_or_languages:
                __constructor_pre_hooks__[constructor_type][language] = method_in
    return set_pre_hook

def reregister_pre_hook(original_type: str,
                        original_language: Language,
                        constructor_type: str,
                        language_or_languages: language_or_list_of_languages):
    desired_method = __constructor_pre_hooks__[original_type][original_language]
    register_pre_hook(constructor_type, language_or_languages)(desired_method)

def run_constructor_pre_hook(constructor_in: Constructor,
    language_in: Language, context_in: Context, framing_in: Framing) -> Tuple[Constructor, Framing]:
    """ If a pre hook is defined for the constructor in a language, runs that.
    """
    constructor_type: str = constructor_in.constructor_type
    try:
        pre_hook = __constructor_pre_hooks__[constructor_type][language_in]
    except KeyError:
        try:
            pre_hook = __constructor_pre_hooks__[constructor_type][langs.mul_]
        except KeyError:
            return constructor_in, framing_in
    return pre_hook(constructor_in, language_in, context_in, framing_in)

def run_pre_hook(old_constructor: Constructor,
                  old_language: Language,
                  old_context: Context,
                  old_framing: Framing) -> Tuple[Constructor, Framing]:
    new_constructor: Constructor
    new_framing: Framing
    new_constructor, new_framing = run_constructor_pre_hook(old_constructor, old_language, old_context, old_framing)
    while new_constructor != old_constructor:
        old_constructor = new_constructor
        new_constructor, new_framing = run_constructor_pre_hook(old_constructor, old_language, old_context, new_framing)
    return new_constructor, new_framing
# end pre-hook logic

# start post_hook logic
__constructor_post_hooks__ = defaultdict(dict)
def register_post_hook(constructor_type: str,
                       language_or_languages: language_or_list_of_languages):
    """ Registers a hook that is run after the main renderer of this constructor is called.
    """
    if isinstance(language_or_languages, Language):
        def set_post_hook(method_in):
            __constructor_post_hooks__[constructor_type][language_or_languages] = method_in
    else:
        def set_post_hook(method_in):
            for language in language_or_languages:
                __constructor_post_hooks__[constructor_type][language] = method_in
    return set_post_hook

def reregister_post_hook(original_type: str,
                        original_language: Language,
                        constructor_type: str,
                        language_or_languages: language_or_list_of_languages):
    desired_method = __constructor_post_hooks__[original_type][original_language]
    register_post_hook(constructor_type, language_or_languages)(desired_method)

def run_post_hook(constructor_in: Constructor,
                  language_in: Language,
                  context_in: Context,
                  framing_in: Framing,
                  renderer_outputs: RendererOutput) -> Framing:
    """ If a post hook is defined for the constructor in a language, runs that.
    """
    constructor_type: str = constructor_in.constructor_type
    try:
        post_hook = __constructor_post_hooks__[constructor_type][language_in]
    except KeyError:
        try:
            post_hook = __constructor_post_hooks__[constructor_type][langs.mul_]
        except KeyError:
            return renderer_outputs, framing_in
    return post_hook(renderer_outputs, language_in, context_in, framing_in)
# end post-hook logic

__constructor_core_arguments__ = {}
__constructor_type_hierarchy__ = {}
def create_argument_filter(constructor_type: str,
                           constructor_parents: List[str],
                           core_arguments: List[str],
                           argument_filter: Optional[Callable]=default_argument_filter) -> Callable[..., Constructor]:
    __constructor_type_hierarchy__[constructor_type] = constructor_parents

    core_argument_list = []
    for parent in constructor_parents:
        core_argument_list = core_argument_list + __constructor_core_arguments__[parent]
    __constructor_core_arguments__[constructor_type] = core_argument_list + core_arguments

    def custom_argument_filter(*args):
        return argument_filter(constructor_type, core_argument_list + core_arguments, *args)
    return custom_argument_filter

def constructor_subtype(subtype: str, supertype: str) -> bool:
    current_subtype = subtype
    while __constructor_type_hierarchy__[current_subtype]:
        if supertype in __constructor_type_hierarchy__[current_subtype]:
            return True
        elif __constructor_type_hierarchy__[current_subtype]:
            current_subtype = __constructor_type_hierarchy__[current_subtype][0]
    return False

__constructor_argument_eval_orders__ = defaultdict(dict)
def set_argument_evaluation_order(constructor_type: str,
                                  language_or_languages: language_or_list_of_languages):
    """ Registers a core argument evaluation order for a given language or languages.
    """
    if isinstance(language_or_languages, Language):
        def set_arg_eval_order(order):
            if len(order) != len(__constructor_core_arguments__[constructor_type]):
                raise ValueError("Order of arguments for", constructor_type, "not equal to number of arguments")
            __constructor_argument_eval_orders__[constructor_type][language_or_languages] = order
    else:
        def set_arg_eval_order(order):
            if len(order) != len(__constructor_core_arguments__[constructor_type]):
                raise ValueError("Order of arguments for", constructor_type, "not equal to number of arguments")
            for language in language_or_languages:
                __constructor_argument_eval_orders__[language] = order
    return set_arg_eval_order

# start scope logic
def render_scope(constructor_in: Constructor,
                 language_in: Language,
                 context_in: Context,
                 framing_in: Framing) -> Scope:
    """ Renders all scope arguments.
    """
    constructor_type = constructor_in.constructor_type
    renderer_scope = {}
    new_context_scope = context_in.push((constructor_type, 'scope'))
    for scope_object in constructor_in.scope_arguments:
        scope_output = render(scope_object, language_in, new_context_scope, framing_in)
        renderer_scope.update(scope_output.scope.scope)
        if scope_output.catena is not None:
            tracking_config = CatenaConfig({'_tracking': CatenaTracking(constructor_type, 'scope', scope_object.constructor_type)})
            scope_output = RendererOutput(scope_output.catena.add_to_config(tracking_config), scope_output.scope)
            if 'outputs' not in renderer_scope:
                renderer_scope['outputs'] = defaultdict(list)
            renderer_scope['outputs'][scope_object.constructor_type].append(scope_output)
    return Scope(renderer_scope)
# end scope logic

# start core argument logic
def get_argument_evaluation_order(constructor_in: Constructor, language_in: Language) -> list:
    """ If an explicit evaluation order is provided for a constructor, returns that.
    """
    constructor_name = constructor_in.constructor_type
    try:
        argument_evaluation_order = __constructor_argument_eval_orders__[constructor_name][language_in]
    except KeyError:
        argument_evaluation_order = __constructor_core_arguments__[constructor_name]
    return argument_evaluation_order

def render_core_arguments(constructor_in: Constructor,
                          language_in: Language,
                          context_in: Context,
                          framing: Framing) -> Tuple[dict, Framing]:
    argument_evaluation_order = get_argument_evaluation_order(constructor_in, language_in)

    renderer_arguments: dict = {}
    for core_argument in argument_evaluation_order:
        renderer_argument, framing = process_argument(constructor_in,
                                     language_in, context_in, framing,
                                     core_argument)
        renderer_arguments[core_argument] = renderer_argument
    return renderer_arguments, framing

def run_arg_pre_hook(current_argument: Constructor,
                     language_in: Language,
                     context: Context,
                     framing: Framing,
                     constructor_type: str,
                     core_argument: str
                     ) -> Tuple[Constructor, Framing]:
    """ If a pre hook is defined for a particular argument in a language, runs that.
    """
    try:
        arg_pre_hook = __constructor_arg_pre_hooks__[constructor_type][language_in][core_argument]
    except KeyError:
        try:
            arg_pre_hook = __constructor_arg_pre_hooks__[constructor_type][langs.mul_][core_argument]
        except KeyError:
            return current_argument, framing
    return arg_pre_hook(current_argument, language_in, context, framing)

def run_arg_post_hook(current_outputs: RendererOutput,
                      language_in: Language,
                      context: Context,
                      framing: Framing,
                      constructor_type: str,
                      core_argument: str) -> Framing:
    """ If a post hook is defined for a particular argument in a language, runs that.
    """
    try:
        arg_post_hook = __constructor_arg_post_hooks__[constructor_type][language_in][core_argument]
    except KeyError:
        try:
            arg_post_hook = __constructor_arg_post_hooks__[constructor_type][langs.mul_][core_argument]
        except KeyError:
            return framing
    return arg_post_hook(current_outputs, language_in, context, framing)

def render_argument(current_argument: Any,
                    language_in: Language,
                    context: Context,
                    framing: Framing,
                    constructor_type: str,
                    core_argument: str) -> RendererOutput:
    """ Renders an argument of this constructor.
    """
    if not isinstance(current_argument, Constructor):
        return RendererOutput(current_argument, Scope({}))
    current_outputs = render(current_argument, language_in, context, framing)
    tracking_config = CatenaConfig({'_tracking': CatenaTracking(constructor_type, core_argument, current_argument.constructor_type)})
    current_outputs = RendererOutput(current_outputs.catena.add_to_config(tracking_config), current_outputs.scope)
    return current_outputs

def process_argument(constructor_in: Constructor,
                     language_in: Language,
                     context_in: Context,
                     framing: Framing,
                     core_argument: str) -> Tuple[RendererOutput, Framing]:
    """ Renders a core argument, running pre- and post-hooks if they are defined.
    """
    constructor_type, _, _, core_arguments, _ = constructor_in
    current_argument = core_arguments[core_argument]
    new_context = context_in.push((constructor_type, core_argument))

    current_argument, framing = run_arg_pre_hook(current_argument,
                     language_in, new_context, framing,
                     constructor_type, core_argument)

    current_outputs = render_argument(current_argument,
                     language_in, new_context, framing,
                     constructor_type, core_argument)

    framing = run_arg_post_hook(current_outputs,
                      language_in, new_context, framing,
                      constructor_type, core_argument)
    return current_outputs, framing
# end core argument logic

# start main renderer logic
def run_main_renderer(constructor_in: Constructor,
                      language_in: Language,
                      context_in: Context,
                      framing_in: Framing,
                      renderer_arguments: dict,
                      renderer_scope: Scope) -> RendererOutput:
    """ Runs the main renderer for this constructor.
    """
    constructor_type: str = constructor_in.constructor_type
    renderer_inputs: RendererInputs = RendererInputs(context_in, framing_in, renderer_arguments, renderer_scope)
    try:
        current_renderer = __constructor_renderers__[constructor_type][language_in]
    except KeyError:
        try:
            current_renderer = __constructor_renderers__[constructor_type][langs.mul_]
        except KeyError as f:
            raise KeyError(f"No renderer for {constructor_type} in language {language_in}") from f
    renderer_output: RendererOutput = current_renderer(renderer_inputs)
    return renderer_output
# end main renderer logic

def render(constructor_in: Constructor,
           language_in: Language,
           context_in: Optional[Context] = None,
           framing_in: Optional[Framing] = None) -> Union[CatenaZipper, RendererOutput]:
    """ Renders a constructor in language ``language_in``.
    """
    old_constructor: Constructor = constructor_in
    new_constructor: Constructor

    if context_in is None:
        context_in = Context([])

    old_framing: Framing
    if framing_in is None:
        old_framing = Framing({})
    else:
        old_framing = framing_in
    new_framing: Framing

    # Step 1
    new_constructor, new_framing = run_pre_hook(old_constructor, language_in, context_in, old_framing)
    # Step 2
    renderer_scope = render_scope(new_constructor, language_in, context_in, new_framing)
    # Step 3
    renderer_arguments, new_framing = render_core_arguments(new_constructor, language_in, context_in, new_framing)
    # Step 4
    renderer_outputs = run_main_renderer(new_constructor,
                       language_in, context_in, new_framing,
                       renderer_arguments, renderer_scope)
    # Step 5
    renderer_outputs, new_framing = run_post_hook(new_constructor,
                       language_in, context_in, new_framing,
                       renderer_outputs)

    if context_in.is_empty():
        return renderer_outputs.catena
    return renderer_outputs
Constructor.render = render
