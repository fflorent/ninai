""" Collection of utility functions for use across other parts of Ninai.
"""

from functools import singledispatch
from typing import Union

import tfsl.utils
from tfsl import Item, ItemValue, Lexeme, LexemeSense, L, Q_, Statement, Language
from udiron import Catena, CatenaZipper, NonLexicalUnit
from udiron.base.catenazipper import zip_lexeme

import ninai.base.constants as C
from ninai.base.context import Context
from ninai.base.scope import Scope

@singledispatch
def get_id_to_find(entity):
    """ Returns the id of an object to be searched for in a sense graph.
    """
    raise NotImplementedError("Cannot get id of", type(entity))

@get_id_to_find.register
def _(entity: str) -> str:
    if tfsl.utils.matches_item(entity) or tfsl.utils.matches_sense(entity):
        return entity
    elif tfsl.utils.matches_lexeme(entity):
        return L(entity).get_senses()[0]

@get_id_to_find.register(Item)
@get_id_to_find.register(ItemValue)
@get_id_to_find.register(LexemeSense)
def _(entity: Union[Item, ItemValue, LexemeSense]) -> str:
    return entity.id

@get_id_to_find.register
def _(entity: Lexeme) -> str:
    return entity.get_senses()[0]

def is_desired_gender(sense_in, desired_gender: str) -> bool:
    """ Returns True if the lexeme for this sense has the desired grammatical gender.
    """
    lex = L(sense_in)
    gender_stmts = lex[C.grammatical_gender]
    if len(gender_stmts) != 0:
        gender_value = gender_stmts[0].value
        return gender_value == desired_gender
    return False

def gendered(sense_in) -> bool:
    """ True if the lexeme for this sense has a grammatical gender.
    """
    lex = L(sense_in)
    return len(lex[C.grammatical_gender]) != 0

def is_desired_pos(sense_in, desired_pos: str) -> bool:
    if not desired_pos:
        return True
    lex = L(sense_in)
    return lex.category == desired_pos

def catena_from_item(item_in, language_in: Language, **scope) -> CatenaZipper:
    target_label = item_in.get_label(language_in)
    return zip_lexeme(NonLexicalUnit(target_label, language=language_in))

def catena_from_item_id(concept_in, language_in: Language, **scope) -> CatenaZipper:
    """ Returns a Catena containing a NonLexicalUnit with the label of an item as its only form.
    """
    if tfsl.utils.matches_item(concept_in):
        item_in = Q_(concept_in)
        return catena_from_item(item_in, language_in)
    raise KeyError("Could not convert", concept_in, "into Catena")

def cat_outputs(target: dict, source: dict) -> dict:
    """ Concatenates two values for a particular key.
        Perhaps this should be turned into a customizer for pydash.merge_with?
    """
    for k, v in source.items():
        if k in target.keys():
            target[k] += v
        else:
            target[k] = v
    return target

def cat_scope_dicts(*scope_dicts: Scope) -> Scope:
    """ Intended to combine an arbitrary list of scope dictionaries into a single scope dictionary.
        Perhaps this should be substituted with pydash.merge_with?
    """
    output_dict = {}
    for scope_dict in scope_dicts:
        for k, v in scope_dict.scope.items():
            if k in output_dict:
                if k == 'outputs':
                    output_dict['outputs'] = cat_outputs(output_dict['outputs'], scope_dict['outputs'])
                else:
                    output_dict[k] = v
            else:
                output_dict[k] = v
    return Scope(output_dict)

def local_top_level(context: Context) -> bool:
    """ Returns True if the context provided indicates that we're at some sort of top level in processing a predicate.
    """
    top_level = lambda item: item[1] != 'scope' and item[0] != 'Backref'
    return not any(top_level(item) for item in context.context)

def originating_parent_type(catena: CatenaZipper) -> type:
    """ Returns the type of the parent of the constructor that generated this Catena.
    """
    current_config = catena.get_config()
    return current_config.get('_tracking').parent_type

def originating_parent_argument(catena: CatenaZipper) -> str:
    """ Returns the name of the argument that this Catena was rendered as.
    """
    current_config = catena.get_config()
    return current_config.get('_tracking').parent_argument

def originating_constructor(catena: CatenaZipper) -> type:
    """ Returns the type of the constructor that generated this Catena.
    """
    current_config = catena.get_config()
    return current_config.get('_tracking').originator

def get_qualifier_values(stmt: Statement, qualifier: str) -> list:
    """ Returns a list of values for a particular qualifier of a given statement.
        This should ideally be agnostic to the type of the values, but right now a Wikibase entity with an id is assumed.
    """
    return [claim.value.id for claim in stmt[qualifier]]

def get_gender_stmts(subject: Union[str, Item]) -> list:
    """ Returns the P21 values of a given subject item.
    """
    if isinstance(subject, str):
        current_object = Q_(subject)
    else:
        current_object = subject
    return current_object[C.sex_or_gender]

# TODO: for names, have a utility function to assemble from name lexemes?
