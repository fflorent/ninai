""" Definition of the Action constructor and separate handling for rendering one.
"""

from typing import Optional, Tuple

from tfsl import Language, langs
from udiron import CatenaZipper

import ninai.base.graph_client as graph
import ninai.base.constants as C
import ninai.base.utility as U
from ninai.base.constructor import Constructor, RendererOutput, create_argument_filter, register_argument_pre_hook, handle_identifiers
from ninai.base.context import Context
from ninai.base.framing import Framing
from ninai.base.identifier import Identifier

def action_argument_filter(constructor_type: str, core_argument_list: str, *args) -> Constructor:
    """ Treats the first argument passed in as the predicate.
    """
    identifier: Optional[str] = None
    child_identifiers: dict = {}
    scope_arguments: list = []

    predicate: str = args[0]
    for argument in args[1:]:
        if isinstance(argument, Identifier):
            identifier = argument.identifier
            child_identifiers[identifier] = (tuple(), constructor_type)
            continue
        else:
            child_identifiers = handle_identifiers(argument, 'scope_' + argument.constructor_type, child_identifiers)
            scope_arguments.append(argument)
    return Constructor(constructor_type, identifier, child_identifiers, {"predicate": predicate}, scope_arguments)

def get_thematic_relation_spec(predicate_catena: CatenaZipper) -> dict:
    """ Obtains the thematic relations specified on the sense of a particular predicate catena.
    """
    thematic_relation_stmts = predicate_catena.get_sense()[C.has_thematic_relation]
    thematic_relation_spec = {}
    for stmt in thematic_relation_stmts:
        thematic_role_item = stmt.value
        if thematic_role_item is False:
            thematic_relation_spec['None'] = stmt
        else:
            thematic_role = thematic_role_item.id
            thematic_relation_spec[thematic_role] = stmt
    return thematic_relation_spec

@register_argument_pre_hook("Action", langs.mul_, "predicate")
def get_predicate_catena(current_argument: str,
                     language_in: Language, new_context: Context, framing: Framing) -> Tuple[CatenaZipper, Framing]:
    """ Creates a Catena based on the sense passed in as the Action's predicate.
    """
    id_to_find = U.get_id_to_find(current_argument)

    predicate_catena = graph.find_predicate(id_to_find, language_in, **framing.framing)
    framing = framing.set('thematic_relations', get_thematic_relation_spec(predicate_catena))

    return predicate_catena, framing

Action = create_argument_filter("Action", [], ["predicate"], argument_filter=action_argument_filter)
