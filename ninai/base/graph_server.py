""" Holds logic for traversing sense networks using NetworkX.
"""

import configparser
import os
import time
from pathlib import Path
from xmlrpc.server import SimpleXMLRPCServer

import networkx as nx
import SPARQLWrapper
import tfsl.utils

import ninai.base.constants as C

PREFIX_WD = 'http://www.wikidata.org/entity/'
PREFIX_P = 'http://www.wikidata.org/prop/direct/'
WDQS_ENDPOINT = 'https://query.wikidata.org/sparql'
query_str_sense_to_item = """
select ?i ?ilang ?j (wd:Q55872899 as ?jlang) {{
    ?i ^ontolex:sense/dct:language ?ilang.
    {{ ?i wdt:{} ?j }}
}}
"""
query_str_sense_to_sense = """
select ?i ?ilang ?j ?jlang {{
    ?i ^ontolex:sense/dct:language ?ilang.
    {{ ?i wdt:{} ?j .
       ?j ^ontolex:sense/dct:language ?jlang }}
}}
"""

# TODO: migrate this out when config is needed for more than just graph stuff
def read_config_server():
    """ Reads the config file residing at /path/to/ninai/config.ini.
    """
    config = configparser.ConfigParser()
    current_config_path = (Path(__file__).parent / '../../config.ini').resolve()
    config.read(current_config_path)
    cpath = config['Ninai']['CachePath']
    ttl = float(config['Ninai']['TimeToLive'])
    port = int(config['Ninai']['GraphServerPort'])
    return cpath, ttl, port

cache_path, time_to_live, graph_server_port = read_config_server()
os.makedirs(cache_path,exist_ok=True)

def get_filename(property_name):
    """ Constructs the name of a text file containing a sense subgraph based on a given property.
    """
    return os.path.join(cache_path, f"subgraph_{property_name}.txt")

def build_relations(property_name):
    """ Returns a list of relations by querying the Wikidata Query Service and saving the output to a file.
    """
    query_str = query_str_sense_to_item if property_name in [C.item_for_this_sense, C.predicate_for, C.demonym_of] else query_str_sense_to_sense
    sparql = SPARQLWrapper.SPARQLWrapper(WDQS_ENDPOINT, agent='ninai 0.0.1')
    sparql.setQuery(query_str.format(property_name))
    sparql.setReturnFormat(SPARQLWrapper.JSON)
    relations_out = sparql.query().convert()
    relations = []
    with open(get_filename(property_name), 'w') as relations_file:
        relations_file.write('\t'.join(relations_out['head']['vars']) + '\n')
        for relation in relations_out['results']['bindings']:
            new_relation = []
            for column_name in ['i', 'ilang', 'j', 'jlang']:
                new_relation.append(relation[column_name]['value'])
            relations.append(new_relation)
            relations_file.write('\t'.join(new_relation) + '\n')
    return relations

def read_relations_from_file(filename):
    """ Returns a list of relations from a file.
    """
    assert time.time() - os.path.getmtime(filename) < time_to_live
    with open(filename) as fileptr:
        next(fileptr)
        relations = []
        for line in fileptr:
            components = line.rstrip('\n').split('\t')
            relations.append(components)
    return relations

def obtain_relations(property_name):
    """ Obtains a sense relation graph, one way or another.
    """
    try:
        filename = get_filename(property_name)
        relations = read_relations_from_file(filename)
    except (FileNotFoundError, OSError, AssertionError):
        relations = build_relations(property_name)
    return relations

target_property_list = [C.translation, C.synonym, C.antonym, C.troponym_of, C.hyperonym, C.pertainym, C.item_for_this_sense, C.predicate_for, C.demonym_of]

def get_translation_networks():
    """ Constructs the translation networks used by the other constructors.
    """
    relations = {}
    for property_name in target_property_list:
        relations[property_name] = obtain_relations(property_name)
    total_graph = build_total_graph(relations)
    return build_translation_networks(total_graph)

def build_total_graph(relations):
    """ Turns a list of relationships into a list of directed subgraphs.
    """
    G = nx.DiGraph()
    for prop, relation_list in relations.items():
        prop = prop.replace(PREFIX_P, '')
        for relation in relation_list:
            subj, subjlang, obj, objlang = [thing.replace(PREFIX_WD, '') for thing in relation]
            G.add_node(subj, lang=subjlang)
            G.add_node(obj, lang=objlang)
            G.add_edge(subj, obj, pred=prop)
            if objlang == C.wikidatan:
                G.add_edge(obj, subj, pred=prop)
    return G

def filter_function(total_graph, allowed_properties):
    def filter_function_out(n1, n2):
        return total_graph[n1][n2]['pred'] in allowed_properties
    return filter_function_out

def build_translation_network(total_graph, property_list):
    """ Composes a set of subgraphs into a single unified graph.
    """
    return nx.subgraph_view(total_graph, filter_edge=filter_function(total_graph, property_list))

def build_translation_networks(total_graph):
    """ Constructs all three translation networks.
    """
    predicate_network = build_translation_network(total_graph, [C.predicate_for, C.translation, C.synonym])
    substantive_network = build_translation_network(total_graph, [C.item_for_this_sense, C.translation, C.synonym])
    demonym_network = build_translation_network(total_graph, [C.demonym_of, C.translation, C.synonym])
    return {
        "predicate": predicate_network,
        "substantive": substantive_network,
        "demonym": demonym_network
    }

network_mappings = get_translation_networks()

def get_shortest_paths(network, concept_in: str, language_id: str):
    """ Searches the provided network for all senses in the provided language linked to the
        provided concept.
    """
    candidate_paths = {}
    shortest_paths = nx.shortest_path(network, concept_in)
    for node in shortest_paths:
        if network.nodes[node]['lang'] == language_id:
            pathgraph = nx.path_graph(shortest_paths[node])
            candidate_paths[node] = [(network.edges[s, t], t) for (s, t) in pathgraph.edges()]
    return candidate_paths

def search_graph(concept_in: str, network_in: str, language_in: str):
    """ Searches the provided network for all senses in the provided language linked to the
        provided concept, but also adding the concept itself if it happens to be a sense in the
        provided language.
    """
    current_network = network_mappings[network_in]
    try:
        candidate_paths = get_shortest_paths(current_network, concept_in, language_in)
    except nx.exception.NodeNotFound:
        if tfsl.utils.matches_sense(concept_in):
            candidate_paths = {concept_in: []}
        else:
            candidate_paths = {}
    return candidate_paths

if __name__ == '__main__':
    graph_server = SimpleXMLRPCServer(("localhost", graph_server_port))
    print(f"Listening on port {graph_server_port}...")
    graph_server.register_function(search_graph, 'search_graph')
    graph_server.serve_forever()
