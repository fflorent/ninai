""" Basic definition of the Concept.
    Most functionality of interest to the handling of Concepts resides in ninai.base.graph.
"""

from typing import Tuple

from tfsl import Language, langs

import ninai.base.graph_client as graph
import ninai.base.utility as U
from ninai.base.context import Context
from ninai.base.framing import Framing
from ninai.base.constructor import Constructor, create_argument_filter, RendererInputs, RendererOutput, register_renderer, register_pre_hook
from ninai.base.identifier import Identifier
from ninai.base.scope import Scope

def concept_argument_filter(constructor_type: str, core_argument_list: str, *args) -> Constructor:
    """ Argument filter for concepts.
    """
    identifier: str = ""
    scope_arguments: list = []
    core_arguments: dict = {}
    child_identifiers: dict = {}

    core_arguments[core_argument_list[0]] = args[0]
    for argument in args[1:]:
        if isinstance(argument, Identifier):
            identifier = argument.identifier
        else:
            scope_arguments.append(argument)
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments)

@register_pre_hook("Concept", langs.mul_)
def concept_pre_hook(constructor_in: Constructor,
                     language_in: Language,
                     context_in: Context,
                     framing_in: Framing) -> Tuple[Constructor, Framing]:
    constructor_type, identifier, child_ids, core_args, scope_args = constructor_in
    if "language" in core_args:
        return constructor_in, framing_in
    core_args_mod = core_args.copy()
    core_args_mod["language"] = language_in
    new_constructor = Constructor(constructor_type, identifier, child_ids, core_args_mod, scope_args)
    return new_constructor, framing_in

@register_renderer("Concept", langs.mul_)
def concept_render(inputs: RendererInputs) -> RendererOutput:
    """ Provides a Catena representing ``object_in`` in language ``language_in``.
    """
    _, framing, arguments, _ = inputs
    id_to_find = U.get_id_to_find(arguments["entity_id"].catena)

    language, _ = arguments["language"]
    out = graph.find_sense(id_to_find, language, fallback=U.catena_from_item_id, **framing.framing) # TODO adjust kwargs
    return RendererOutput(out, Scope({}))

Concept = create_argument_filter("Concept", [], ["entity_id", "language"], argument_filter=concept_argument_filter)
