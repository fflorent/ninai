import configparser
import random
import xmlrpc.client
from pathlib import Path

from tfsl import L, langs
from udiron import Catena, CatenaConfig, CatenaZipper, DependentEntry, DependentList

import ninai.base.constants as C

__refine_candidate_paths__ = {}

def read_config_client():
    """ Reads the config file residing at /path/to/ninai/config.ini.
    """
    config = configparser.ConfigParser()
    current_config_path = (Path(__file__).parent / '../../config.ini').resolve()
    config.read(current_config_path)
    port = int(config['Ninai']['GraphServerPort'])
    return port

graph_server_port = read_config_client()

def register_sense_refine(language_in):
    """ Registers a method by which a list of candidate sense paths may be refined for a given language.
    """
    def stuff(method_in):
        __refine_candidate_paths__[language_in] = method_in
        return method_in
    return stuff

def process_candidates(candidate_paths, language_in, **scope):
    """ Processes the candidate paths to find a suitable sense, and then constructs a Catena with that sense.
    """
    new_config = CatenaConfig({})

    if len(candidate_paths) == 0:
        raise KeyError('How did you even get to this point?')
    elif len(candidate_paths) != 1:
        if refine_function := __refine_candidate_paths__.get(language_in, False):
            candidate_paths, new_config = refine_function(candidate_paths, new_config, **scope)
    min_path_length = min([len(v) for k, v in candidate_paths.items()])
    candidate_paths = [k for k, v in candidate_paths.items() if len(v) == min_path_length]
    sense_id = random.choice(candidate_paths)
    new_lexeme = L(sense_id)
    return CatenaZipper([DependentEntry(Catena(
        new_lexeme,
        new_lexeme.language,
        new_lexeme[sense_id],
        frozenset(),
        new_config,
        DependentList([], [])
    ), C.root_node)])

def find_or_fallback(concept_in, network_in, language_in, fallback=None, **scope):
    """ Returns a Catena representing something, or constructs one with 'fallback' if no sense is found
        and a fallback is passed in.
    """
    with xmlrpc.client.ServerProxy(f"http://localhost:{graph_server_port}/") as proxy:
        candidate_paths = proxy.search_graph(concept_in, network_in, language_in.item)
    if len(candidate_paths) == 0:
        if fallback:
            return fallback(concept_in, language_in, **scope)
        else:
            raise KeyError(f"Could not create Catena for {concept_in} in {language_in}")
    return process_candidates(candidate_paths, language_in, **scope)

def find_sense(concept_in, language_in, fallback=None, **scope):
    """ Returns a Catena representing a Concept.
    """
    return find_or_fallback(concept_in, "substantive", language_in, fallback, **scope)

def find_demonym(concept_in, language_in, fallback=None, **scope):
    """ Returns a Catena representing a demonym.
    """
    return find_or_fallback(concept_in, "demonym", language_in, fallback, **scope)

def find_predicate(concept_in, language_in, fallback=None, **scope):
    """ Returns a Catena representing a predicate.
    """
    return find_or_fallback(concept_in, "predicate", language_in, fallback, **scope)
