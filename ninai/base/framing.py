""" Holder for the Framing class.
"""

from typing import Any, NamedTuple, Optional

class Framing(NamedTuple):
    """ Configuration object (top-down and lateral transfer) for rendering Constructors.
        At the moment this is merely a wrapper around a Python dictionary.
    """
    framing: dict

def framing_get(self: Framing, key: str, default: Optional[Any] = None) -> Any:
    return self.framing.get(key, default)
Framing.get = framing_get

def framing_set(self: Framing, key: str, value: Any) -> Framing:
    framing_new = self.framing.copy()
    framing_new[key] = value
    return Framing(framing_new)
Framing.set = framing_set

def framing_del(self: Framing, key: str) -> Framing:
    framing_new = self.framing.copy()
    del framing_new[key]
    return Framing(framing_new)
Framing.delete = framing_del
