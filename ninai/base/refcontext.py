""" Holds the RefContext class and a number of other classes which make
    sense only when a RefContext is rendered.
"""

import re
from functools import singledispatchmethod
from typing import Any, Callable, List, NamedTuple, Optional, Tuple

from funcy import any_fn, merge
from tfsl import Item, ItemValue, Language, Statement
from tfsl.languages import langs, get_first_lang
from tfsl.utils import matches_item, matches_property, matches_lexeme, matches_form, matches_sense

import ninai.base.constants as C
import ninai.base.utility as U
from ninai.base.constructor import Constructor, RendererInputs, RendererOutput, create_argument_filter, register_pre_hook, register_renderer
from ninai.base.constructorzipper import ConstructorZipper, zip_constructor
from ninai.base.context import Context
from ninai.base.identifier import Identifier
from ninai.base.framing import Framing
from ninai.base.scope import Scope

class Setf(NamedTuple):
    reference: str
    operation: List[str]

class Declaration(NamedTuple):
    name: str
    initial_value: Item

# start refcontext logic
def refcontext_argument_filter(constructor_type: str, core_argument_list: List[str], *args) -> Constructor:
    """ Argument filter for constructors, assuming they were not declared with
        a custom argument filter.
    """
    identifier: str = ""
    scope_arguments: list = []
    core_arguments: dict = {}
    child_identifiers: dict = {}
    command_list: list = []

    for argument in args:
        if isinstance(argument, Identifier):
            identifier = argument.identifier
        else:
            command_list.append(argument)
    core_arguments["commands"] = command_list
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments)

@register_pre_hook("RefContext", langs.mul_)
def refcontext_pre_hook(constructor_in: Constructor,
                     language_in: Language,
                     context_in: Context,
                     framing_in: Framing) -> Tuple[Constructor, Framing]:
    constructor_type, identifier, child_ids, core_args, scope_args = constructor_in
    if "language" in core_args:
        return constructor_in, framing_in
    core_args_mod = core_args.copy()
    core_args_mod["language"] = language_in
    new_constructor = Constructor(constructor_type, identifier, child_ids, core_args_mod, scope_args)
    return new_constructor, framing_in

def process_command(command, references: dict) -> dict:
    if isinstance(command, Declaration):
        return process_declaration(command, references)
    elif isinstance(command, Setf):
        return process_setf(command, references)
    else:
        raise TypeError(f"Can't process {type(command)} in RefContext")

def process_declaration(arg: Declaration, references: dict) -> dict:
    name, initial_value = arg
    references[name] = initial_value
    return references

def process_setf(arg: Setf, references: dict) -> dict:
    lhs, operands = arg
    operation = operands[0]
    if matches_property(operation):
        value = operands[1]
        if valid_itemvalue_value(value):
            rhs = ItemValue(value)
        else:
            rhs = value
        references[lhs] += Statement(operation, rhs)
    elif langcode := re.match(r"L([a-z-]+)", operation):
        label = operands[1] @ get_first_lang(langcode.group(1))
        references[lhs] = references[lhs].add_label(label)
    elif operation == "wrap":
        toplevel_constructor = lhs
        target_identifier, wrapping = operands[1:]
        references = wrap_constructor(toplevel_constructor, target_identifier, wrapping, references)
    return references

def wrap_constructor(toplevel_name: str, target_identifier: str, wrapping: Callable, references: dict) -> dict:
    toplevel_constructor = zip_constructor(references[toplevel_name])
    target_constructor = toplevel_constructor.find(target_identifier)

    target_constructor = target_constructor.wrap(wrapping)

    arg, _ = target_constructor.root()
    references[toplevel_name] = arg
    return references

@register_renderer("RefContext", langs.mul_)
def refcontext_render(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    commands = arguments["commands"].catena
    language, _ = arguments["language"]

    current_references: dict = {}
    statements: list = []
    basic_framing = {C.FRAMING_CONTEXT: current_references}

    for command in commands:
        if isinstance(command, Constructor):
            output = command.render(language, context, Framing(merge(basic_framing, framing.framing)))
            statements.append(output)
        else:
            basic_framing = {C.FRAMING_CONTEXT: process_command(command, current_references)}
    return RendererOutput(statements, Scope(current_references))

RefContext = create_argument_filter("RefContext", [], ["commands", "language"], argument_filter=refcontext_argument_filter)
# end refcontext logic

valid_itemvalue_value = any_fn(matches_item, matches_property, matches_lexeme, matches_form, matches_sense)

# start backref logic

def backref_argument_filter(constructor_type: str, core_argument_list: List[str], *args) -> Constructor:
    """ Argument filter for constructors, assuming they were not declared with
        a custom argument filter.
    """
    identifier: str = ""
    scope_arguments: list = []
    core_arguments: dict = {}
    child_identifiers: dict = {}

    identifier = args[0]
    core_arguments["identifier"] = args[0]
    for argument in args[1:]:
        if isinstance(argument, Identifier):
            identifier = argument.identifier
        else:
            scope_arguments.append(argument)
    return Constructor(constructor_type, identifier, child_identifiers, core_arguments, scope_arguments)

@register_pre_hook("Backref", langs.mul_)
def backref_pre_hook(constructor_in: Constructor,
                     language_in: Language,
                     context_in: Context,
                     framing_in: Framing) -> Tuple[Constructor, Framing]:
    constructor_type, identifier, child_ids, core_args, scope_args = constructor_in
    if "language" in core_args:
        return constructor_in, framing_in
    core_args_mod = core_args.copy()
    core_args_mod["language"] = language_in
    new_constructor = Constructor(constructor_type, identifier, child_ids, core_args_mod, scope_args)
    return new_constructor, framing_in

@register_renderer("Backref", langs.mul_)
def backref_render(inputs: RendererInputs) -> RendererOutput:
    context, framing, arguments, scope = inputs
    language, _ = arguments["language"]
    identifier, _ = arguments["identifier"]
    target_object = framing.get(C.FRAMING_CONTEXT)[identifier]
    if isinstance(target_object, Constructor):
        new_context = context.push(("Backref", "identifier"))
        return target_object.render(language, new_context, framing)
    else:
        return RendererOutput(U.catena_from_item(target_object, language), Scope({}))

Backref = create_argument_filter("Backref", [], ["identifier", "language"], argument_filter=backref_argument_filter)
# end backref logic