""" Holder for the Context class.
"""

from typing import NamedTuple, Tuple

class Context(NamedTuple):
    """ Tracks the current position of the constructor being rendered relative to the root.
        At the moment this is merely a wrapper around a Python list.
    """
    context: list

def context_push(self: Context, entry: Tuple[str, str]) -> Context:
    return Context(self.context + [entry])
Context.push = context_push

def context_is_empty(self: Context) -> bool:
    return not self.context
Context.is_empty = context_is_empty
