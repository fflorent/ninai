""" Holder for the Scope class.
"""

from typing import Any, NamedTuple, Optional

class Scope(NamedTuple):
    """ Configuration object (bottom-up transfer) for rendering Constructors.
        At the moment this is merely a wrapper around a Python dictionary.
    """
    scope: dict

def scope_get(self: Scope, arg: str, default: Optional[Any] = None) -> Any:
    return self.scope.get(arg, default)
Scope.get = scope_get