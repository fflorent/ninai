""" Holds the Identifier class.
"""

from typing import NamedTuple

class Identifier(NamedTuple):
    identifier: str
