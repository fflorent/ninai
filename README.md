# Ninai

* கண்தெரிந்து வழி நடக்கும்படி நினை. (Take care that you walk with your eyes open.)
* ஒரு நன்றி செய்தவரை உள்ள அளவும் நினை. (Think of those who have done you even one favour as long as you live.)

([translations above by Peter Percival](https://en.wikisource.org/wiki/Tamil_Proverbs))

## About

Ninai (from the classical Tamil for 'to think') is a statement construction system that uses, among other things, Wikidata items and lexemes with other grammatical primitives for the construction of complex assertions that can be rendered using [Udiron](https://gitlab.com/mahir256/udiron/) into a given language.

(The author of this project is willing to relinquish the name Ninai to the set of constructors that will actually be in use on the Abstract Wikipedia, whether or not derived from what is developed here.)

## Setup

(The code here was developed with Python 3.9 and 3.10.)

Clone this repository:

```
git clone https://gitlab.com/mahir256/ninai
```

Without changing into the newly created directory, also clone Udiron and tfsl
(using the Phabricator URL for tfsl, since it interfaces with Wikimedia projects directly):

```
git clone https://gitlab.com/mahir256/udiron
git clone https://phabricator.wikimedia.org/source/tool-twofivesixlex.git tfsl
```

Now make sure these end up in your PYTHONPATH. The simplest way to do this below is for a Unix system, where the paths must be substituted accordingly:

```
export PYTHONPATH=$PYTHONPATH:/path/to/tfsl:/path/to/udiron:/path/to/ninai
```

Now install the dependencies of all three of these, again substituting the paths accordingly:

```
pip install -r /path/to/tfsl/requirements.txt
pip install -r /path/to/udiron/requirements.txt
pip install -r /path/to/ninai/requirements.txt
```

Now rename 'config.ini.example' to 'config.ini' and specify
1) where the translation network should be stored ('CachePath') and
2) how long (in seconds) this network should be stored before regeneration ('TimeToLive').

## Use

The auto-generated documentation of Ninai may be found [here](doc/modules.rst).

Some examples of Ninai's use may be found [here](demonstrations.md).

There are two primary units of composition: the Constructor and the Concept.

* A Concept is a container for a Wikidata item, a Wikidata lexeme, or a sense on such a lexeme. When a Concept is rendered, a path from the contained item or sense (or for a lexeme, the first sense therein) to senses in the target language is found, and the lexeme containing that sense is used in the resulting syntax tree. (This process currently fails if the local translation network lacks a connection to an appropriate sense in the target language from the entity contained in the Concept.)
* A Constructor is a container for arguments to that constructor. On its own, it does nothing; when rendered, however, a language-specific constructor-specific function (or 'renderer') processes those arguments and returns some output, which can be either an output syntax tree and some contextual information or just the contextual information without the tree.

[More information about design choices may be found here](design_choices.md).

## Licensing

All code herein is Apache 2.0 unless otherwise stated below.
