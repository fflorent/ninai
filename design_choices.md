# Design choices

This page serves as a general list of descriptions of motivations for different design choices across Ninai and Udiron.

## General choices

Ideally every function (except possibly utility functions with applications across languages), at a sufficiently high level, should have its functionality overridable through some sort of extra input. In both Ninai and Udiron this input is provided to these functions through the passing of extra keyword arguments, typically an unpacked Python dictionary.

Additionally every function should yield some sort of default output when there is not enough information provided to force one particular formulation or rendered output. This output should tend toward the formal, the neutral, the most generally applicable, or random if none of the previous is actually achievable. What follows from this is that if a particular output of a particular constructor combination in a particular language is desired, the appropriate amount of contextual information must be provided such that the execution paths which lead to that output are consistently taken.

It is problematic that the localization of function names is not quite feasible in Python, given that in principle the constructors should be usable regardless of the language of the desired output. While the names are currently all in English for uniformity, it is hoped that the migration of these to Wikifunctions ZObjects, which are to have localizable labels/aliases, will remedy this situation quickly.

## Ninai-specific

What can have a constructor is certainly an open question; one may declare a constructor for *any* semantically meaningful element provided that a renderer for it in some language is properly written afterwards. As far as what is provided by Ninai, the primary trend is to prefer the inclusion of constructors whose meanings may be treated metalinguistically to those less so treatable. This includes, but is not limited to, the following:

* all tenses, aspects, moods, persons, genders, numbers, and thematic relations,
* all locative modifiers that occur sufficiently commonly either as grammatical cases or as markings with single adpositions,
* sufficiently common constructions that can manifest without the use of verbs, and
* sufficiently common items that can occur in discourse.

The names of constructors are generally chosen so as to reflect, with as much detachment from specific phrasing in a language as possible, the function they are intended to represent. Thus a constructor's name may be `Possession`, describing the relationship between its arguments, rather than the English word sequence "has a". Similarly the constructors for locative elements are named similarly to grammatical cases which describe the same relationship in other languages, rather than using English prepositions directly: `Subessive` instead of "below", `Adessive` instead of "near".

## Udiron-specific

Udiron creates and manipulates syntax trees inspired in large part by the Universal Dependencies (UD) scheme. Since this scheme has already been applied to many languages (122 as of February 2022), it is hoped that speakers of those languages can make better decisions about how their renderers can process syntax trees, and that the harmonization of syntax trees induced by that scheme will be easier to match through the reuse of utility functions across languages.

The connection is that of 'inspiration', rather than of 'conformance', since only Wikidata QIDs rather than UD relations are used to link different syntax tree nodes, and individual affixes and word components may be processed as separate syntax tree nodes if required for the processing of a particular language (where in UD, for example, a circumfix may be split into multiple separate components, or a prefix may be inseparable from the word it attaches to).
