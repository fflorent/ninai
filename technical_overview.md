# Technical description

This page provides in some order information about the technical backend of Ninai/Udiron. Where differences between the textual description here and the current code exist, the code should supersede this text.

Because tfsl is envisioned to exist as built-in handling/processing of items and lexemes on Wikifunctions, details about that project will not be emphasized as much here, although references to classes in that library (using the `tfsl.` prefix) should be construed as references to objects containing information from them.

## Document structure

### Notes on terminology

Throughout this document 'Constructor' refers to the type detailed in the subsection of that name below, while 'constructor' refers to a particular function--an 'argument filter', as named in the section "Writing them" below--that yields a Constructor.

### Notes on Python types

The names of certain Python types may be used even if within context they must be mapped to other nearly equivalent things--`dict` (rather than `typing.Mapping` or `typing.Dict`) may be referred to even if the result of a Typed map (Z883) call is meant, and `list` (rather than `typing.List`) may be referred to even if the result of a Typed list (Z881) call is meant.

### Notes on text formatting

Text in {brackets} refers to Python-specific considerations that, assuming things work out properly, should not apply to ports of this code to Wikifunctions.

Text in < brackets > refers to Wikifunctions-specific considerations (to the best of my understanding) that are not dealt with the same way in this Python implementation.

## Basic types

Because types in Wikifunctions are not classes in the object-oriented programming sense, most types in Ninai/Udiron consist only of fields with static types.
{This is achieved by requiring most types to subclass only from typing.NamedTuple and specifying the types of their fields inline.}

At the moment Ninai defines twelve types and Udiron defines six. The most important among these types are described below.

### Constructor

The `Constructor` type is a container for information about a particular constructor. It consists of the following five fields:

* `constructor_type`, a string denoting the constructor which originated this Constructor (and thus which set of functions to use when rendering this Constructor);
* `identifier`, an `Identifier` object (currently a wrapper around a string) which allows the Constructor to be referred to in other functions;
* `child_identifiers`, a dict mapping the identifiers of this Constructor's core/scope arguments (and their core/scope arguments) to the path that needs to be taken in the Constructor tree to reach them from this Constructor;
* `core_arguments`, a dict mapping the names of core arguments to the arguments themselves (whether strings, Constructors, or anything else); and
* `scope_arguments`, a list of arguments beyond the core arguments passed to a constructor.

These objects are not created on their own, instead being generated through argument filters--see "Writing them" below.

### Catena

The `Catena` type is a container for information about a particular node in a dependency-based syntax tree. It is named for a particular syntactic unit (Q1287636), although information about the node highest-up in an actual catena is stored in this container, with information about that node's dependents placed in separate Catenae and linked from that node.

A Catena consists of the following six fields:

* `lexeme`, a `tfsl.Lexeme` which is the base overall lexical element of the node represented by this Catena;
* `language`, a `tfsl.Language` < or natural language (Z60) > indicating which language's Udiron functions to use when rendering this Catena;
* `sense`, a `tfsl.LexemeSense` which is the specific semantic element represented by this Catena--typically a sense on the `lexeme`;
* `inflections`, a set of strings for Qids, roughly akin to the set of grammatical features on a Wikidata lexeme form (but by no means required to be such features);
* `config`, a `CatenaConfig` (currently a wrapper around a dict) mapping arbitrary strings to arbitrary objects, used for controlling the behavior of Udiron functions; and
* `dependents`, a `DependentList` (currently a wrapper around two lists) containing the dependents to the left/right in the syntax tree of the node represented by this Catena.

Like Constructors, Catenae are not created on their own, instead being generated through other user-facing functions.

### Information transfer receptacles

There are a number of other smaller classes, at this point merely wrapping other types, that are defined in the interest of organizing information transfer in constructor rendering:

* `Context`, a list of tuples which behaves like a call stack during constructor rendering < which may not be necessary if this can be introspected >, where each tuple contains 1) the name of a constructor and 2) either the name of a core argument or the name of a constructor prefixed with 'scope_' for scope arguments;
* `Framing`, a dict mapping arbitrary strings to arbitrary objects, used for lateral or downward information transfer; and
* `Scope`, a dict similar to framing but used for upward information transfer.

## Writing and using constructors

### Defining them

{The syntax for defining a constructor is as follows:

`constructor_name = create_argument_filter(constructor_type, constructor_parents, core_argument_list, argument_filter = default_argument_filter)`

}

The function above does different things with its arguments:

* it specifies 'parentage' of constructors, akin to a subtyping relationship in other programming languages, depending on the list of constructor parents given for a constructor type;
* it specifies the names of core arguments according to the core argument list passed in *along with the names of the core arguments of its constructor parents*; and
* it returns a closure, the 'argument filter', based on the constructor type, the names of the core arguments, and (if specified) the actual function passed in {as a named argument}.

< The constructor parentage tree and the core argument mappings may be stored as ZObjects. >

An 'argument filter' essentially does what it says on the tin, checking each of the arguments provided, performing transformations of them if needed, and returning a Constructor at the end. Most constructors should be able to make do with argument filters based on the current default filter, which filters `Identifier`s from everything else and separates core and scope arguments.

### Using them

The syntax for invoking a constructor is as follows:

`constructor_name(argument_1, argument_2, argument_3, ...)`

where any number of arguments may be provided. The first set among these are treated as 'core arguments', in that they are mapped to the names set when defining the constructor, while the remainder are treated as 'scope arguments'.

The order of the core arguments is of course significant, while constructors should not typically rely on the order of passed-in scope arguments.

### Special constructors: Concept

The `Concept` constructor is the most direct link when using constructors to the lexicographical data on Wikidata. It takes a single argument (either a `tfsl.Item`, a `tfsl.Lexeme`, or a string with either a Qid or an Lid) and is meant to return an appropriate Catena at render time.

### Special constructors: Action

The `Action` constructor is in part a link to the lexicographical data on Wikidata. Its first argument (a string with either a Qid or an Lid) is mapped at render time to a Catena containing an appropriate predicate, and all other arguments are treated as modifiers of that predicate (specifying things like Catenae mapping to thematic relations, spatiotemporal indications, or subordinate clauses, or other kinds of information like tense, aspect, or mood).

### Special constructors: RefContext

The `RefContext` constructor is an initial attempt at creating an environment for the customization of items and constructors that reference those items. At render time it creates (as of this writing) of a dictionary containing arbitrary objects (typically `tfsl.Item`s) and a list of output Catenae. The constructor's arguments are treated as a list of commands, where at render time (as of this writing) each command is interpreted in sequence as follows:

* If a command is a `Declaration`, then the string which is the first argument of that Declaration is set in the RefContext's object dictionary to the object which is that Declaration's second argument.
* If a command is a `Setf`, then the object referred to by that Setf's first argument is modified according to the list of parameters in that Setf's second argument. (This is named after the Common Lisp function.)
* If a command is a Constructor, that constructor is rendered and the resulting Catena appended to the output list.
* If a command is a `Backref`, then the object to which that Backref refers is rendered if the object is a Constructor. (I have not yet decided what to do if the Backref refers to a non-Constructor.)
** Note that `Backref`s may also be used inside Constructors that are invoked inside a RefContext in place of other arguments; they are useless outside a RefContext.

## Constructor rendering

The rendering process for a constructor typically yields a single Catena for a syntax tree. It can be split into five main steps:

1. the pre-hook (if it exists);
2. the rendering of scope arguments (essentially this constructor rendering process for each such argument), the outputs from these combined into a `Scope` object;
3. for each core argument:
   * the argument pre-hook (if it exists);
   * the rendering of that argument (essentially this constructor rendering process); and
   * the argument post-hook (if it exists);
4. the main renderer (but read on about the constructor pre-hook); and then
5. the post-hook (if it exists).

The main renderer and the hooks are the sorts of things contributors will need to write for their languages. < It is my hope that this is made easier through better code organization right now and the switch to the composition of functions format when porting to Wikifunctions. >

### Constructor pre-hook

The pre-hook for a Constructor is provided the Constructor itself as well as the `tfsl.Language` < or Natural language (Z60) > into which it's being rendered, the initial Context, and the initial Framing. It returns another Constructor and a modified Framing.

In the rendering process, if the returned other Constructor is not the exact same as the Constructor that was passed in, this step is over; otherwise, the pre-hook of *that* constructor is run, and a similar check is performed on the result of that pre-hook, and so on. (If a pre-hook is not defined for a constructor, the exact same constructor is indeed returned, such that the initial check passes and execution can continue.)

Because the Constructor returned from the pre-hook need not be the same Constructor that was passed in, we might make a distinction between 'basic' constructors, which at least have main renderers defined, and 'meta-constructors', which can get away with only a pre-hook defined. Since the pre-hook is defined on a language-specific basis, it is possible for a constructor to be a meta-constructor for one set of languages and a basic constructor for others. (The argument filter for that constructor may need to be modified as well.)

{Constructor pre-hooks are registered via decoration with `@register_pre_hook`.} < The contents of `__constructor_pre_hooks__` will reside in a separate ZObject (or separate ZObjects if the single one becomes too unwieldy to edit later). >

### Core argument pre-hook

The pre-hook for a core argument is provided the Constructor for that argument as well as the `tfsl.Language` < or Natural language (Z60) > into which it's being rendered, the current Context, and the current Framing. It returns a Constructor and a modified Framing.

Unlike the constructor pre-hook, there is (currently) no check that the returned Constructor is different from the one passed in. Beyond this, however, similar considerations to the constructor pre-hook apply.

{Core-argument pre-hooks are registered via decoration with `@register_argument_pre_hook`.} < The contents of `__constructor_arg_pre_hooks__` will reside in a separate ZObject (or separate ZObjects if the single one becomes too unwieldy to edit later). >

### Core argument post-hook

The post-hook for a core argument is provided the output from rendering that argument as well as the `tfsl.Language` < or Natural language (Z60) > into which it's being rendered, the current Context, and the current Framing. It returns a modified Framing.

These usually do some clean-up after running the argument pre-hook.

{Core-argument post-hooks are registered via decoration with `@register_argument_post_hook`.} < The contents of `__constructor_arg_post_hooks__` will reside in a separate ZObject (or separate ZObjects if the single one becomes too unwieldy to edit later). >

### Main renderer

The main renderer for a core argument is provided a `RendererInputs`, containing the current Context and Framing along with 1) a dictionary mapping core argument names to the outputs from rendering those arguments and 2) the current Scope. It returns a `RendererOutput` containing a Catena (usually) and another Scope.

Most functionality here is typically outsourced to Udiron functions, although this of course need not be the case.

{Main renderers are registered via decoration with `@register_renderer`.} < The contents of `__constructor_renderers__` will reside in a separate ZObject (or separate ZObjects if the single one becomes too unwieldy to edit later). >

### Constructor post-hook

The post-hook for a Constructor is provided the output from running the main renderer as well as the `tfsl.Language` < or Natural language (Z60) > into which it's being rendered, the initial Context, and the initial Framing. It returns a modified Framing.

For constructors corresponding to predicates, things like adding full stops might go here, although the range of functionality that may belong here is by no means limited to that. (Just as the core argument post-hook could clean up after the core argument pre-hook, so too could the Constructor post-hook clean up after the Constructor pre-hook.)

{Constructor post-hooks are registered via decoration with `@register_post_hook`.} < The contents of `__constructor_post_hooks__` will reside in a separate ZObject (or separate ZObjects if the single one becomes too unwieldy to edit later). >

## Catena rendering

As noted at the start of the last section, the output from a renderer is typically a Catena representing the root of a syntax tree. The rendering process for a Catena yields a textual (string) output.

### Form selection

TODO

### Form building

TODO

### Surface joining

TODO

### Surface transformation

TODO
