# Demonstrations of Ninai

This is a list of demonstration utterances.
Most work in Bengali, and many of the same work in Swedish;
a few of these concern German, and one is in French.

For information about the individual constructors, see [the auto-generated documentation](doc/modules.rst).

## Jupiter

These build up to the sentence "Jupiter is the largest planet in the solar system.",
the first example sentence with regard to Abstract Wikipedia rendering,
in Bengali and Swedish.

```python
from tfsl import L, Q, langs

import ninai.renderers
from ninai import Concept
from ninai.constructors import *

for LANG in [langs.bn_, langs.sv_]:
    for sentence in [
        Identification(Concept("Q319"), Concept("Q634")),
        Identification(Concept("Q319"), Instance(Concept("Q634"))),
        Identification(Concept("Q319"), Instance(Concept("Q634"), Definite())),
        Attribution(Concept("Q634"), Concept("Q59863338")),
        Attribution(Instance(Concept("Q634")), Concept("Q59863338")),
        Attribution(Instance(Concept("Q634"), Definite()), Concept("Q59863338")),
        Identification(Concept("Q319"), Instance(Attribution(Concept("Q634"), Concept("Q59863338")))),
        Identification(Concept("Q319"), Instance(Attribution(Concept("Q634"), Concept("Q59863338")), Definite())),
        Identification(Concept("Q319"), Instance(Attribution(Concept("Q634"), Superlative(Concept("Q59863338"))), Definite())),
        Identification(Concept("Q319"), Instance(Attribution(Concept("Q634"), Superlative(Concept("Q59863338"), Locative(Concept("Q544")))), Definite()))
    ]:
        print(sentence(LANG))
```

The expected output from the above should look something like the following:

```
বৃহস্পতি গ্রহ।
বৃহস্পতি একটা গ্রহ।
বৃহস্পতি গ্রহটি।
গ্রহ বড়।
একটা গ্রহ বড়।
গ্রহটা বড়।
বৃহস্পতি একটি বড় গ্রহ।
বৃহস্পতি বড় গ্রহটা।
বৃহস্পতি সবচেয়ে বড় গ্রহটি।
বৃহস্পতি সৌরমণ্ডল সবচেয়ে বড় গ্রহটি।
Jupiter är planet.
Jupiter är en planet.
Jupiter är klotet.
Planet är stor.
Ett klot är stort.
Klotet är stort.
Jupiter är en stor planet.
Jupiter är det stora klotet.
Jupiter är det största klotet.
Jupiter är den största planeten i solsystemet.
```

## Kishore Kumar (1)

This attempts to render the opening lines of [this song](https://www.youtube.com/watch?v=KigjSXLBorc)
(and adds a variant thereof)
in Bengali and Swedish.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept
from ninai.constructors import *

for LANG in [langs.bn_,langs.sv_]:
    for sentence in [
        Existence(Concept("Q190507"), PastTense()),
        Existence(Concept("Q316"), PastTense()),
        Negation(Existence(Concept("Q190507"), Hodiernal())),
        Negation(Existence(Concept("Q316"), Hodiernal())),
        Existence(Concept("Q190507"), Crastinal()),
        Existence(Concept("Q316"), Crastinal())
    ]:
        print(sentence(LANG))
```

The expected output from the above should look something like the following:

```
আশা ছিল।
ভালোবাসা ছিল।
আজ আশা নেই।
আজ ভালোবাসা নেই।
আগামীকাল আশা থাকবে।
আগামীকাল ভালোবাসা থাকবে।
Det fanns hopp.
Det fanns kärlek.
Det finns inte hopp i dag.
Det finns inte kärlek i dag.
Det kommer att finnas hopp i morgon.
Det kommer att finnas kärlek i morgon.
```

## Kishore Kumar (2)

This attempts to render the opening lines of [this song](https://www.youtube.com/watch?v=07BadGcIEe4)
in Bengali.

```python
from tfsl import Q, langs
import ninai.renderers.bn
from ninai import Concept
from ninai.constructors import *

for sentence in [
    Attribution(Concept("Q575"), Concept("Q3635662"), PastTense(), Locative(Distal(Concept("Q575"), Emphasis()))),
    Existence(Concept("Q1075"), Locative(Attribution(Concept("Q7391292"), Concept("Q2765162"))), PastTense())
]:
    print(sentence(langs.bn_))
```

The expected output from the above should look something like the following:

```
সেই রাতে রাত পূর্ণিমা ছিল।
ফাল্গুনী হাওয়ায় রং ছিল।
```

## Descriptions of places and people

This was created by request of User:Nikki
for German.

```python
from tfsl import Q, langs
import ninai.renderers.de
from ninai import Concept
from ninai.concepts import GeneralDemonym
from ninai.constructors import *

for sentence in [
    Identification(Concept("Q4191"), Instance(Attribution(Concept("Q515"), Locative(Concept("Q39"))))),
    Identification(Concept("Q980"), Instance(Attribution(Concept("Q1221156"), Locative(Concept("Q183"))))),
    Identification(Concept("Q42"), Instance(Attribution(Concept("Q36180"), GeneralDemonym("Q145"))), PastTense()),
    Identification(Concept("Q564328"), Instance(Attribution(Concept("Q82955"), GeneralDemonym("Q183")))),
]:
    print(sentence(langs.de_))
```

The expected output from the above should look something like the following:

```
Luzern ist eine Stadt in der Schweiz.
Bayern ist ein Bundesland in Deutschland.
Douglas Adams war ein britischer Schriftsteller.
Annalena Baerbock ist eine deutsche Politikerin.
```

## Interactions between objects

This produces some contrived but grammatical sentences involving an airplane and a bell
in Bengali and Swedish.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept, Action
from ninai.constructors import *

for LANG in [langs.bn_, langs.sv_]:
    for sentence in [
        Action("Q69396773", Agent(Concept("Q197")), Patient(Concept("Q101401")), Hesternal()),
        Action("Q69396773", Agent(Concept("Q197")), Hodiernal(), Patient(Concept("Q101401"))),
        Action("Q69396773", Crastinal(), Patient(Concept("Q101401")), Agent(Concept("Q197"))),
        Action("Q206021", Agent(Concept("Q197")), Hesternal()),
        Action("Q206021", Hodiernal(), Agent(Concept("Q197"))),
        Action("Q206021", Agent(Concept("Q197")), Crastinal())
    ]:
        print(sentence(LANG))
```

The expected output from the above should look something like the following:

```
গতকাল আকাশযান ঘণ্টাকে ঢেকা মারল।
আজ আকাশযান ঘণ্টাকে ঢেকা মারে।
আগামীকাল আকাশযান ঘণ্টাকে ঢেকা মারবে।
গতকাল আকাশযান উড়ল।
আজ আকাশযান ওড়ে।
আগামীকাল আকাশযান উড়বে।
Flygplan sköt klocka i går.
Flygplan skjuter klocka i dag.
Flygplan kommer att skjuta klocka i morgon.
Flygplan flög i går.
Flygplan letar i dag.
Flygplan kommer att leta i morgon.
```

## Wegnehmen, arbeiten, and schneien

This first produces some sentences meant to exhibit the handling of German separable verb prefixes,
then produces some sentences exhibiting intransitive verb handling
and then produces one sentences using a verb which lacks a semantic agent.

```python
from tfsl import L, Q, langs
import ninai.renderers.de
from ninai import Concept, Action
from ninai.constructors import *

for sentence in [
    Action(L(617591)["S2"], Agent(Instance(Concept("Q155647"), Definite())), Hesternal(), Topic(Concept("Q187880")), Source(Instance(Concept("Q326653"), Definite()))),
    Action(L(617591)["S2"], Agent(Instance(Concept("Q155647"), Definite())), Topic(Concept("Q187880")), Hodiernal(), Source(Instance(Concept("Q326653"), Definite()))),
    Action(L(617591)["S2"], Agent(Instance(Concept("Q155647"), Definite())), Topic(Concept("Q187880")), Source(Instance(Concept("Q326653"), Definite())), Crastinal()),
    Action(L(617591)["S1"], Agent(Instance(Concept("Q155647"), Definite())), Topic(Concept("Q187880")), Hesternal()),
    Action(L(617591)["S1"], Agent(Instance(Concept("Q155647"), Definite())), Hodiernal(), Topic(Concept("Q187880"))),
    Action(L(617591)["S1"], Crastinal(), Agent(Instance(Concept("Q155647"), Definite())), Topic(Concept("Q187880"))),
    Action("Q6958747", Hesternal(), Agent(Instance(Concept("Q852389"), Definite()))),
    Action("Q6958747", Crastinal(), Agent(Instance(Concept("Q5113"), Definite()))),
    Action("Q25931702", Agent(Instance(Concept("Q852389"), Definite())), Hodiernal()),
    Action("Q75078783", Crastinal())
]:
    print(sentence(langs.de_))
```

The expected output from the above should look something like the following:

```
Der Astrologe nahm gestern dem Buchhalter Excalibur weg.
Der Astrologe nimmt heute dem Buchhalter Excalibur weg.
Der Astrologe wird morgen dem Buchhalter Excalibur wegnehmen.
Der Astrologe nahm gestern Excalibur weg.
Der Astrologe nimmt heute Excalibur weg.
Der Astrologe wird morgen Excalibur wegnehmen.
Der Imker arbeitete gestern.
Der Vogel wird morgen arbeiten.
Der Imker schläft heute.
Es wird morgen schneien.
```

## Ring Ring, bara...

This produces some sentences pertaining to calling someone on the phone and with different subjects.

```python
from tfsl import Q, langs
import ninai.renderers
from ninai import Concept, Action
from ninai.constructors import *

for sentence in [
    Action("Q2296401", Agent(Speaker()), PastTense()),
    Action("Q2296401", Agent(Listener(Familiar())), PastTense()),
    Action("Q2296401", Agent(Listener(Formal())), PastTense()),
    Action("Q2296401", Agent(Listener(Formal())), PresentTense()),
    Action("Q2296401", Agent(Listener(Formal())), FutureTense()),
]:
    for LANG in [langs.sv_, langs.bn_]:
        print(sentence(LANG))
```

The expected output from the above should look something like the following:

```
Jag slog en signal.
আমি ফোন করলাম।
Du slog en signal.
তুমি ফোন করলে।
Ni slog en signal.
আপনি ফোন করলেন।
Ni slår en signal.
আপনি ফোন করেন।
Ni kommer att slå en signal.
আপনি ফোন করবেন।
```

## Mauvais et Bougie

This produces some simple sentences pertaining to moving, at the request of User:VIGNERON.

```python
from tfsl import Q, langs
import ninai.renderers.fr
from ninai import Concept, Action
from ninai.constructors import *

for sentence in [
    Action("Q17988854", Agent(Speaker()), PresentTense()),
    Action("Q17988854", Agent(Listener(Singular(), Familiar())), PresentTense()),
    Action("Q17988854", Agent(SpeakerOthers()), PresentTense()),
    Action("Q17988854", Agent(Listener(Plural())), PresentTense())
]:
    for LANG in [langs.fr_]:
        print(sentence(LANG))
```

The expected output from the above should look something like the following:

```
Je bouge.
Tu bouges.
Nous bougeons.
Vous bougez.
```
