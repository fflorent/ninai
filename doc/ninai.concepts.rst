
ninai.concepts package
**********************


Module contents
===============

Holds, for the moment, the GeneralDemonym class only. (If more Concept
subclasses become necessary, they will reside here as well.)

**ninai.concepts.generaldemonym_render(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Provides a Catena representing ``object_in`` in language
   ``language_in``.
