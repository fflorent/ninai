
ninai.base package
******************


Submodules
==========


ninai.base.action module
========================

Definition of the Action constructor and separate handling for
rendering one.

**ninai.base.action.get_thematic_relation_spec(predicate_catena:
udiron.base.catenazipper.CatenaZipper) -> dict**

   Obtains the thematic relations specified on the sense of a
   particular predicate catena.


ninai.base.concept module
=========================

Basic definition of the Concept. Most functionality of interest to the
handling of Concepts resides in ninai.base.graph.

**ninai.base.concept.concept_render(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<#ninai.base.constructor.RendererOutput>`_**

   Provides a Catena representing ``object_in`` in language
   ``language_in``.


ninai.base.constants module
===========================

Some constants for use with Ninai only. Any constants added here which
later may find use in Udiron should be moved to udiron.base.constants.


ninai.base.constructor module
=============================

Holds the Constructor class, container classes for renderer
inputs/outputs, and the bulk of the logic for rendering Constructors.

**class ninai.base.constructor.CatenaTracking(parent_type: type,
parent_argument: str, originator: type)**

   Bases: ``NamedTuple``

   Container holding information about the constructor origin of a
   particular Catena.

   **_asdict()**

      Return a new dict which maps field names to their values.

   ``_field_defaults = {}``

   ``_fields = ('parent_type', 'parent_argument', 'originator')``

   **classmethod _make(iterable)**

      Make a new CatenaTracking object from a sequence or iterable

   **_replace(**kwds)**

      Return a new CatenaTracking object replacing specified fields
      with new values

   ``originator: type``

      The type of the constructor that yielded this Catena.

   ``parent_argument: str``

      The argument position of the constructor that yielded this
      Catena.

   ``parent_type: type``

      The type of the parent of the constructor that yielded this
      Catena.

**class ninai.base.constructor.Constructor(constructor_type,
identifier, child_identifiers, core_arguments, scope_arguments)**

   Bases: ``NamedTuple``

   **_asdict()**

      Return a new dict which maps field names to their values.

   ``_field_defaults = {}``

   ``_fields = ('constructor_type', ... ts', 'scope_arguments')``

   **classmethod _make(iterable)**

      Make a new Constructor object from a sequence or iterable

   **_replace(**kwds)**

      Return a new Constructor object replacing specified fields with
      new values

   ``child_identifiers: dict``

      Alias for field number 2

   ``constructor_type: str``

      Alias for field number 0

   ``core_arguments: dict``

      Alias for field number 3

   ``identifier: ninai.base.identifier.Identifier``

      Alias for field number 1

   **render(language_in: tfsl.languages.Language, context_in:
   Optional[ninai.base.context.Context] = None, framing_in:
   Optional[ninai.base.framing.Framing] = None) ->
   Union[udiron.base.catenazipper.CatenaZipper,
   `ninai.base.constructor.RendererOutput
   <#ninai.base.constructor.RendererOutput>`_]**

      Renders a constructor in language ``language_in``.

   ``scope_arguments: list``

      Alias for field number 4

**class ninai.base.constructor.RendererInputs(context, framing,
arguments, scope)**

   Bases: ``NamedTuple``

   **_asdict()**

      Return a new dict which maps field names to their values.

   ``_field_defaults = {}``

   ``_fields = ('context', 'framing', 'arguments', 'scope')``

   **classmethod _make(iterable)**

      Make a new RendererInputs object from a sequence or iterable

   **_replace(**kwds)**

      Return a new RendererInputs object replacing specified fields
      with new values

   ``arguments: dict``

      Alias for field number 2

   ``context: ninai.base.context.Context``

      Alias for field number 0

   ``framing: ninai.base.framing.Framing``

      Alias for field number 1

   ``scope: ninai.base.scope.Scope``

      Alias for field number 3

**class ninai.base.constructor.RendererOutput(catena:
udiron.base.catenazipper.CatenaZipper, scope:
ninai.base.scope.Scope)**

   Bases: ``NamedTuple``

   Container holding the output from, and scope information related
   to, a renderer.

   **_asdict()**

      Return a new dict which maps field names to their values.

   ``_field_defaults = {}``

   ``_fields = ('catena', 'scope')``

   **classmethod _make(iterable)**

      Make a new RendererOutput object from a sequence or iterable

   **_replace(**kwds)**

      Return a new RendererOutput object replacing specified fields
      with new values

   ``catena: udiron.base.catenazipper.CatenaZipper``

      Alias for field number 0

   ``scope: ninai.base.scope.Scope``

      Alias for field number 1

**ninai.base.constructor.constructor_call(self:
ninai.base.constructor.Constructor, language: tfsl.languages.Language)
-> str**

**ninai.base.constructor.constructor_subtype(subtype: str, supertype:
str) -> bool**

**ninai.base.constructor.get_argument_evaluation_order(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language) -> list**

   If an explicit evaluation order is provided for a constructor,
   returns that.

**ninai.base.constructor.handle_identifiers(arg:
ninai.base.constructor.Constructor, role: str, child_identifiers:
dict) -> dict**

   If the argument has an identifier, then it and the child
   identifiers known to the argument are saved.

**ninai.base.constructor.merge_identifier_dicts(child_identifier_dict:
dict, parent_identifier_dict: dict, role: str) -> dict**

**ninai.base.constructor.process_argument(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in: ninai.base.context.Context,
framing: ninai.base.framing.Framing, core_argument: str) ->
Tuple[`ninai.base.constructor.RendererOutput
<#ninai.base.constructor.RendererOutput>`_,
ninai.base.framing.Framing]**

   Renders a core argument, running pre- and post-hooks if they are
   defined.

**ninai.base.constructor.register_argument_post_hook(constructor_type:
str, language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]], argument: str)**

   Registers a hook that is run after a particular argument to this
   constructor is rendered.

**ninai.base.constructor.register_argument_pre_hook(constructor_type:
str, language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]], argument: str)**

   Registers a hook that is run before a particular argument to this
   constructor is rendered.

**ninai.base.constructor.register_post_hook(constructor_type: str,
language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

   Registers a hook that is run after the main renderer of this
   constructor is called.

**ninai.base.constructor.register_pre_hook(constructor_type: str,
language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

   Registers a hook that is run before rendering any arguments of this
   constructor.

**ninai.base.constructor.register_renderer(constructor_type: str,
language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

   Registers a renderer with a particular language or languages.

**ninai.base.constructor.render(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in:
Optional[ninai.base.context.Context] = None, framing_in:
Optional[ninai.base.framing.Framing] = None) ->
Union[udiron.base.catenazipper.CatenaZipper,
`ninai.base.constructor.RendererOutput
<#ninai.base.constructor.RendererOutput>`_]**

   Renders a constructor in language ``language_in``.

**ninai.base.constructor.render_argument(current_argument: Any,
language_in: tfsl.languages.Language, context:
ninai.base.context.Context, framing: ninai.base.framing.Framing,
constructor_type: str, core_argument: str) ->
`ninai.base.constructor.RendererOutput
<#ninai.base.constructor.RendererOutput>`_**

   Renders an argument of this constructor.

**ninai.base.constructor.render_core_arguments(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in: ninai.base.context.Context,
framing: ninai.base.framing.Framing) -> Tuple[dict,
ninai.base.framing.Framing]**

**ninai.base.constructor.render_scope(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in: ninai.base.context.Context,
framing_in: ninai.base.framing.Framing) -> ninai.base.scope.Scope**

   Renders all scope arguments.

**ninai.base.constructor.reregister_post_hook(original_type: str,
original_language: tfsl.languages.Language, constructor_type: str,
language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

**ninai.base.constructor.reregister_pre_hook(original_type: str,
original_language: tfsl.languages.Language, constructor_type: str,
language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

**ninai.base.constructor.reregister_renderer(original_type: str,
original_language: tfsl.languages.Language, constructor_type: str,
language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

**ninai.base.constructor.run_arg_post_hook(current_outputs:
ninai.base.constructor.RendererOutput, language_in:
tfsl.languages.Language, context: ninai.base.context.Context, framing:
ninai.base.framing.Framing, constructor_type: str, core_argument: str)
-> ninai.base.framing.Framing**

   If a post hook is defined for a particular argument in a language,
   runs that.

**ninai.base.constructor.run_arg_pre_hook(current_argument:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context: ninai.base.context.Context, framing:
ninai.base.framing.Framing, constructor_type: str, core_argument: str)
-> Tuple[`ninai.base.constructor.Constructor
<#ninai.base.constructor.Constructor>`_, ninai.base.framing.Framing]**

   If a pre hook is defined for a particular argument in a language,
   runs that.

**ninai.base.constructor.run_constructor_pre_hook(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in: ninai.base.context.Context,
framing_in: ninai.base.framing.Framing) ->
Tuple[`ninai.base.constructor.Constructor
<#ninai.base.constructor.Constructor>`_, ninai.base.framing.Framing]**

   If a pre hook is defined for the constructor in a language, runs
   that.

**ninai.base.constructor.run_main_renderer(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in: ninai.base.context.Context,
framing_in: ninai.base.framing.Framing, renderer_arguments: dict,
renderer_scope: ninai.base.scope.Scope) ->
`ninai.base.constructor.RendererOutput
<#ninai.base.constructor.RendererOutput>`_**

   Runs the main renderer for this constructor.

**ninai.base.constructor.run_post_hook(constructor_in:
ninai.base.constructor.Constructor, language_in:
tfsl.languages.Language, context_in: ninai.base.context.Context,
framing_in: ninai.base.framing.Framing, renderer_outputs:
ninai.base.constructor.RendererOutput) -> ninai.base.framing.Framing**

   If a post hook is defined for the constructor in a language, runs
   that.

**ninai.base.constructor.run_pre_hook(old_constructor:
ninai.base.constructor.Constructor, old_language:
tfsl.languages.Language, old_context: ninai.base.context.Context,
old_framing: ninai.base.framing.Framing) ->
Tuple[`ninai.base.constructor.Constructor
<#ninai.base.constructor.Constructor>`_, ninai.base.framing.Framing]**

**ninai.base.constructor.set_argument_evaluation_order(constructor_type:
str, language_or_languages: Union[tfsl.languages.Language,
List[tfsl.languages.Language]])**

   Registers a core argument evaluation order for a given language or
   languages.


ninai.base.graph_client module
==============================

**ninai.base.graph_client.find_demonym(concept_in, language_in,
fallback=None, **scope)**

   Returns a Catena representing a demonym.

**ninai.base.graph_client.find_or_fallback(concept_in, network_in,
language_in, fallback=None, **scope)**

   Returns a Catena representing something, or constructs one with
   ‘fallback’ if no sense is found and a fallback is passed in.

**ninai.base.graph_client.find_predicate(concept_in, language_in,
fallback=None, **scope)**

   Returns a Catena representing a predicate.

**ninai.base.graph_client.find_sense(concept_in, language_in,
fallback=None, **scope)**

   Returns a Catena representing a Concept.

**ninai.base.graph_client.process_candidates(candidate_paths,
language_in, **scope)**

   Processes the candidate paths to find a suitable sense, and then
   constructs a Catena with that sense.

**ninai.base.graph_client.read_config_client()**

   Reads the config file residing at /path/to/ninai/config.ini.

**ninai.base.graph_client.register_sense_refine(language_in)**

   Registers a method by which a list of candidate sense paths may be
   refined for a given language.


ninai.base.graph_server module
==============================

Holds logic for traversing sense networks using NetworkX.

**ninai.base.graph_server.build_relations(property_name)**

   Returns a list of relations by querying the Wikidata Query Service
   and saving the output to a file.

**ninai.base.graph_server.build_total_graph(relations)**

   Turns a list of relationships into a list of directed subgraphs.

**ninai.base.graph_server.build_translation_network(total_graph,
property_list)**

   Composes a set of subgraphs into a single unified graph.

**ninai.base.graph_server.build_translation_networks(total_graph)**

   Constructs all three translation networks.

**ninai.base.graph_server.filter_function(total_graph,
allowed_properties)**

**ninai.base.graph_server.get_filename(property_name)**

   Constructs the name of a text file containing a sense subgraph
   based on a given property.

**ninai.base.graph_server.get_shortest_paths(network, concept_in: str,
language_id: str)**

   Searches the provided network for all senses in the provided
   language linked to the provided concept.

**ninai.base.graph_server.get_translation_networks()**

   Constructs the translation networks used by the other constructors.

**ninai.base.graph_server.obtain_relations(property_name)**

   Obtains a sense relation graph, one way or another.

**ninai.base.graph_server.read_config_server()**

   Reads the config file residing at /path/to/ninai/config.ini.

**ninai.base.graph_server.read_relations_from_file(filename)**

   Returns a list of relations from a file.

**ninai.base.graph_server.search_graph(concept_in: str, network_in:
str, language_in: str)**

   Searches the provided network for all senses in the provided
   language linked to the provided concept, but also adding the
   concept itself if it happens to be a sense in the provided
   language.


ninai.base.utility module
=========================

Collection of utility functions for use across other parts of Ninai.

**ninai.base.utility._(entity: tfsl.lexeme.Lexeme) -> str**

**ninai.base.utility.cat_outputs(target: dict, source: dict) -> dict**

   Concatenates two values for a particular key. Perhaps this should
   be turned into a customizer for pydash.merge_with?

**ninai.base.utility.cat_scope_dicts(*scope_dicts:
ninai.base.scope.Scope) -> ninai.base.scope.Scope**

   Intended to combine an arbitrary list of scope dictionaries into a
   single scope dictionary. Perhaps this should be substituted with
   pydash.merge_with?

**ninai.base.utility.catena_from_item(item_in, language_in:
tfsl.languages.Language, **scope) ->
udiron.base.catenazipper.CatenaZipper**

**ninai.base.utility.catena_from_item_id(concept_in, language_in:
tfsl.languages.Language, **scope) ->
udiron.base.catenazipper.CatenaZipper**

   Returns a Catena containing a NonLexicalUnit with the label of an
   item as its only form.

**ninai.base.utility.gendered(sense_in) -> bool**

   True if the lexeme for this sense has a grammatical gender.

**ninai.base.utility.get_gender_stmts(subject: Union[str,
tfsl.item.Item]) -> list**

   Returns the P21 values of a given subject item.

**ninai.base.utility.get_id_to_find(entity)**

   Returns the id of an object to be searched for in a sense graph.

**ninai.base.utility.get_qualifier_values(stmt:
tfsl.statement.Statement, qualifier: str) -> list**

   Returns a list of values for a particular qualifier of a given
   statement. This should ideally be agnostic to the type of the
   values, but right now a Wikibase entity with an id is assumed.

**ninai.base.utility.is_desired_gender(sense_in, desired_gender: str)
-> bool**

   Returns True if the lexeme for this sense has the desired
   grammatical gender.

**ninai.base.utility.is_desired_pos(sense_in, desired_pos: str) ->
bool**

**ninai.base.utility.local_top_level(context:
ninai.base.context.Context) -> bool**

   Returns True if the context provided indicates that we’re at some
   sort of top level in processing a predicate.

**ninai.base.utility.originating_constructor(catena:
udiron.base.catenazipper.CatenaZipper) -> type**

   Returns the type of the constructor that generated this Catena.

**ninai.base.utility.originating_parent_argument(catena:
udiron.base.catenazipper.CatenaZipper) -> str**

   Returns the name of the argument that this Catena was rendered as.

**ninai.base.utility.originating_parent_type(catena:
udiron.base.catenazipper.CatenaZipper) -> type**

   Returns the type of the parent of the constructor that generated
   this Catena.


Module contents
===============
