
ninai
*****

*  `ninai package <ninai.rst>`_
   *  `Subpackages <ninai.rst#subpackages>`_
      *  `ninai.base package <ninai.base.rst>`_
         *  `Submodules <ninai.base.rst#submodules>`_
         *  `ninai.base.action module
            <ninai.base.rst#module-ninai.base.action>`_
         *  `ninai.base.concept module
            <ninai.base.rst#module-ninai.base.concept>`_
         *  `ninai.base.constants module
            <ninai.base.rst#module-ninai.base.constants>`_
         *  `ninai.base.constructor module
            <ninai.base.rst#module-ninai.base.constructor>`_
         *  `ninai.base.graph_client module
            <ninai.base.rst#module-ninai.base.graph_client>`_
         *  `ninai.base.graph_server module
            <ninai.base.rst#module-ninai.base.graph_server>`_
         *  `ninai.base.utility module
            <ninai.base.rst#module-ninai.base.utility>`_
         *  `Module contents <ninai.base.rst#module-ninai.base>`_
      *  `ninai.concepts package <ninai.concepts.rst>`_
         *  `Module contents
            <ninai.concepts.rst#module-ninai.concepts>`_
      *  `ninai.constructors package <ninai.constructors.rst>`_
         *  `Submodules <ninai.constructors.rst#submodules>`_
         *  `ninai.constructors.locatives module
            <ninai.constructors.rst#module-ninai.constructors.locatives>`_
         *  `ninai.constructors.modifiers module
            <ninai.constructors.rst#module-ninai.constructors.modifiers>`_
         *  `ninai.constructors.nominals module
            <ninai.constructors.rst#module-ninai.constructors.nominals>`_
         *  `ninai.constructors.nonverbal_predicates module
            <ninai.constructors.rst#module-ninai.constructors.nonverbal_predicates>`_
         *  `ninai.constructors.tenses module
            <ninai.constructors.rst#module-ninai.constructors.tenses>`_
         *  `Module contents
            <ninai.constructors.rst#module-ninai.constructors>`_
      *  `ninai.renderers package <ninai.renderers.rst>`_
         *  `Submodules <ninai.renderers.rst#submodules>`_
         *  `ninai.renderers.bn module
            <ninai.renderers.rst#module-ninai.renderers.bn>`_
         *  `ninai.renderers.de module
            <ninai.renderers.rst#module-ninai.renderers.de>`_
         *  `ninai.renderers.fr module
            <ninai.renderers.rst#module-ninai.renderers.fr>`_
         *  `ninai.renderers.ig module
            <ninai.renderers.rst#module-ninai.renderers.ig>`_
         *  `ninai.renderers.mul module
            <ninai.renderers.rst#module-ninai.renderers.mul>`_
         *  `ninai.renderers.sv module
            <ninai.renderers.rst#module-ninai.renderers.sv>`_
         *  `Module contents
            <ninai.renderers.rst#module-ninai.renderers>`_
   *  `Module contents <ninai.rst#module-ninai>`_