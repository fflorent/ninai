
ninai.renderers package
***********************


Submodules
==========


ninai.renderers.bn module
=========================

**ninai.renderers.bn.add_full_stop_if_needed(renderer_outputs:
ninai.base.constructor.RendererOutput, context:
ninai.base.context.Context) -> `ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Adds a full stop to a catena if the catena is the output from
   rendering a top-level constructor.

**ninai.renderers.bn.process_scope_outputs(base_catena:
udiron.base.catenazipper.CatenaZipper, outputs: dict) ->
udiron.base.catenazipper.CatenaZipper**

   Processes scope outputs, possibly attaching them to a base catena.
   Perhaps this should discard an output from the dict once it is
   attached to the catena?

**ninai.renderers.bn.render_action_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Action constructor in Bengali. At the moment the
   only qualifiers on ‘has thematic relation’ that are examined are
   ‘requires grammatical feature’ and ‘object has role’, and even with
   the latter only the first value is taken. It also has yet to
   actually check that the predicate needs to be split before doing
   so.

**ninai.renderers.bn.render_attribution_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Attribution constructor in Bengali.

**ninai.renderers.bn.render_comparativeunspecified_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the ComparativeUnspecified constructor in Bengali.

**ninai.renderers.bn.render_distal_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Distal constructor in Bengali.

**ninai.renderers.bn.render_existence_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Existence constructor in Bengali.

**ninai.renderers.bn.render_greeting_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Greeting constructor in Bengali.

**ninai.renderers.bn.render_identification_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Identification constructor in Bengali.

**ninai.renderers.bn.render_instance_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Instance constructor in Bengali.

**ninai.renderers.bn.render_listener_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Speaker constructor in Bengali.

**ninai.renderers.bn.render_locative_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Locative constructor in Bengali.

**ninai.renderers.bn.render_medial_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Medial constructor in Bengali.

**ninai.renderers.bn.render_negation_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Negation constructor in Bengali.

**ninai.renderers.bn.render_possession_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Possession constructor in Bengali.

**ninai.renderers.bn.render_proximal_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Proximal constructor in Bengali.

**ninai.renderers.bn.render_speaker_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Speaker constructor in Bengali.

**ninai.renderers.bn.render_speaker_others_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for constructors involving the speaker and other parties
   in Bengali.

**ninai.renderers.bn.render_superlative_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Superlative constructor in Bengali.

**ninai.renderers.bn.render_supposition_bn(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Supposition constructor in Bengali.


ninai.renderers.de module
=========================

Holds all renderers for German.

**ninai.renderers.de.add_full_stop_if_needed(renderer_outputs:
ninai.base.constructor.RendererOutput, context:
ninai.base.context.Context) -> `ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Adds a full stop to a catena if the catena is the output from
   rendering a top-level constructor.

**ninai.renderers.de.process_scope_outputs(base_catena:
udiron.base.catenazipper.CatenaZipper, outputs: dict) ->
udiron.base.catenazipper.CatenaZipper**

   Processes scope outputs, possibly attaching them to a base catena.
   Perhaps this should discard an output from the dict once it is
   attached to the catena?

**ninai.renderers.de.render_action_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Action constructor in German. At the moment the
   only qualifiers on ‘has thematic relation’ that are examined are
   ‘requires grammatical feature’ and ‘object has role’, and even with
   the latter only the first value is taken.

**ninai.renderers.de.render_adessive(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Adessive constructor in German.

**ninai.renderers.de.render_attribution_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Attribution constructor in German.

**ninai.renderers.de.render_identification_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Identification constructor in German.

**ninai.renderers.de.render_instance_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Instance constructor in German.

**ninai.renderers.de.render_listener_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Listener constructor in German.

**ninai.renderers.de.render_locative_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Locative constructor in German.

**ninai.renderers.de.render_proxillative(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Proxillative constructor in German.

**ninai.renderers.de.render_speaker_de(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Speaker constructor in German.

**ninai.renderers.de.render_speaker_others_de(inputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for a number of constructors representing the speaker and
   others in German.

**ninai.renderers.de.sense_refine_de(candidate_paths, new_config,
**scope)**

   Used to filter a list of candidate senses when rendering a Concept
   in German.


ninai.renderers.fr module
=========================

Holds all renderers for French.

**ninai.renderers.fr.add_full_stop_if_needed(renderer_outputs:
ninai.base.constructor.RendererOutput, context:
ninai.base.context.Context) -> `ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Adds a full stop to a catena if the catena is the output from
   rendering a top-level constructor.

**ninai.renderers.fr.process_scope_outputs(base_catena:
udiron.base.catenazipper.CatenaZipper, outputs: dict) ->
udiron.base.catenazipper.CatenaZipper**

   Processes scope outputs, possibly attaching them to a base catena.
   Perhaps this should discard an output from the dict once it is
   attached to the catena?

**ninai.renderers.fr.render_action_fr(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Action constructor in French. At the moment the
   only qualifiers on ‘has thematic relation’ that are examined are
   ‘requires grammatical feature’ and ‘object has role’, and even with
   the latter only the first value is taken. It also has yet to
   actually check that the predicate needs to be split before doing
   so.

**ninai.renderers.fr.render_identification_fr(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Identification constructor in French.

**ninai.renderers.fr.render_instance_fr(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Instance constructor in French.

**ninai.renderers.fr.render_listener_fr(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Listener constructor in French.

**ninai.renderers.fr.render_locative_fr(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Locative constructor in French.

**ninai.renderers.fr.render_speaker_fr(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Speaker constructor in French.

**ninai.renderers.fr.render_speaker_others_fr(inputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for a number of constructors representing the speaker and
   others in French.


ninai.renderers.ig module
=========================

**ninai.renderers.ig.render_action_ig(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

**ninai.renderers.ig.render_listener_ig(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

**ninai.renderers.ig.render_locative_ig(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Locative constructor in Igbo.

**ninai.renderers.ig.render_speaker_ig(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Speaker constructor in Igbo.

**ninai.renderers.ig.render_speaker_others_ig(inputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**


ninai.renderers.mul module
==========================

Holds renderers for various modifier constructors that may be applied
to all languages.

**ninai.renderers.mul.render_emphasis(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Definite constructor in all languages. Not sure if
   this ought to be handled with typical_framing_handling as well.

**ninai.renderers.mul.typical_framing_handling(constructor_type: str,
scope_in: dict) -> None**

   Defines and registers methods for a constructor as follows: - On a
   rendering pre hook, the scope dict passed in is merged with the
   framing dict provided. - On rendering, if a scope output is
   present, then it is merely returned; otherwise the scope dict is
   returned as an output.


ninai.renderers.sv module
=========================

Holds all renderers for Swedish.

**ninai.renderers.sv.add_full_stop_if_needed(renderer_outputs:
ninai.base.constructor.RendererOutput, context:
ninai.base.context.Context) -> `ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Adds a full stop to a catena if the catena is the output from
   rendering a top-level constructor.

**ninai.renderers.sv.process_scope_outputs(base_catena:
udiron.base.catenazipper.CatenaZipper, outputs: dict) ->
udiron.base.catenazipper.CatenaZipper**

   Processes scope outputs, possibly attaching them to a base catena.
   Perhaps this should discard an output from the dict once it is
   attached to the catena?

**ninai.renderers.sv.propagate_scope_outputs(outputs)**

   Packages scope outputs for upward propagation to the renderer of
   another constructor.

**ninai.renderers.sv.render_action_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Action constructor in French. At the moment the
   only qualifiers on ‘has thematic relation’ that are examined are
   ‘requires grammatical feature’ and ‘object has role’, and even with
   the latter only the first value is taken. It also has yet to
   actually check that the predicate needs to be split before doing
   so.

**ninai.renderers.sv.render_attribution_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Attribution constructor in Swedish.

**ninai.renderers.sv.render_existence_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Existence constructor in Swedish.

**ninai.renderers.sv.render_greeting_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Greeting constructor in Swedish.

**ninai.renderers.sv.render_identification_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Identification constructor in Swedish.

**ninai.renderers.sv.render_instance_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Instance constructor in Swedish.

**ninai.renderers.sv.render_listener_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Listener constructor in Swedish.

**ninai.renderers.sv.render_locative_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Locative constructor in Swedish.

**ninai.renderers.sv.render_near(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer of the Adessive constructor in Swedish.

**ninai.renderers.sv.render_negation_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Negation constructor in Swedish.

**ninai.renderers.sv.render_speaker_others_sv(inputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

**ninai.renderers.sv.render_speaker_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Speaker constructor in Swedish.

**ninai.renderers.sv.render_superlative_sv(inputs:
ninai.base.constructor.RendererInputs) ->
`ninai.base.constructor.RendererOutput
<ninai.base.rst#ninai.base.constructor.RendererOutput>`_**

   Renderer for the Superlative constructor in Swedish.


Module contents
===============

Loads all renderers. To load the renderers for a specific language,
name that module directly: “import ninai.renderers.bn as bn” rather
than “from ninai.renderers import bn”
