
ninai.constructors package
**************************


Submodules
==========


ninai.constructors.locatives module
===================================

Holds constructors for specifying the locations of things and the
locations from where or to where things are going.

**ninai.constructors.locatives.ActionLocation(*args)**

   Base class for the location of a constructor in motion. These may
   be used, for example, as scope arguments of other constructors.

**ninai.constructors.locatives.Adessive(*args)**

   Used to indicate that the containing constructor is near a
   particular object:

   ::

      >>> Attribution(Concept(Q(319)), Adessive(Concept(Q(544)))) # Jupiter is near the solar system.

   (See also general notes regarding StativeLocation.)

**ninai.constructors.locatives.GeneralLocation(*args)**

   Base class for arbitrary locations realized as adpositional
   phrases, whether or not in motion.

**ninai.constructors.locatives.Locative(*args)**

   Used to indicate the generic location of a particular object (akin
   to ‘in’, ‘at’, ‘on’):

   ::

      >>> Attribution(Concept(Q(319)), Location(Concept(Q(544)))) # Jupiter is in the solar system.

   (See also general notes regarding StativeLocation.)

**ninai.constructors.locatives.Proxillative(*args)**

   Used to indicate that the containing constructor is going to (near)
   a particular object:

   ::

      >>> Action("Q19279529", Concept(Q(319)), Proxillative(Concept(Q(544)))) # Jupiter is going to near the solar system.

   (See also general notes regarding ActionLocation.)

**ninai.constructors.locatives.StativeLocation(*args)**

   Base class for the location of a constructor not in motion. These
   may be used, for example, as the second argument of Attribution.

**ninai.constructors.locatives.Subessive(*args)**

   Used to indicate that the containing constructor is below a
   particular object:

   ::

      >>> Attribution(Concept(Q(319)), Subessive(Concept(Q(544)))) # Jupiter is below the solar system.

   (See also general notes regarding StativeLocation.)


ninai.constructors.modifiers module
===================================

**ninai.constructors.modifiers.Comparative(*args)**

   Used to specify that an attribute is more pronounced compared to
   some other object.

   >>> Attribution(Concept(Q(319)), Comparative(Concept(Q(59863338)), Concept(Q(193)))) # Jupiter is larger than Saturn.

**ninai.constructors.modifiers.ComparativeUnspecified(*args)**

   Used to specify that an attribute is more pronounced without
   specifying a standard for comparison:

   >>> Attribution(Concept(Q(319)), ComparativeUnspecified(Concept(Q(59863338)))) # Jupiter is larger.

**ninai.constructors.modifiers.Definite(*args)**

   Used to mark an instance of a concept as definite:

   ::

      >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.
      >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)), Definite())) # Jupiter is the planet.

   There may be instances, even in languages where definiteness is not
   marked, where omitting this constructor may not make a difference,
   but omitting it might yield undesirable or inconsistent results.

**ninai.constructors.modifiers.Demonstrative(*args)**

   Base class for demonstratives.

**ninai.constructors.modifiers.Distal(*args)**

   Used to specify that the contained constructor is away from the
   speaker and the listener (much like ‘ano’ in Japanese):

   ::

      >>> Attribution(Distal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

   (See also general notes regarding Demonstrative.)

**ninai.constructors.modifiers.Emphasis(*args)**

   Metalinguistic emphasis, which might well occur anywhere and be
   rendered in many ways:

   ::

      >>> Identification(Distal(Concept(Q(575))), Concept(Q(3635662))) # সে রাত পূর্ণিমা।
      >>> Identification(Distal(Concept(Q(575)), Emphasis()), Concept(Q(3635662))) # সেই রাত পূর্ণিমা।

   (More examples to come when more requirements of emphasis handling
   arise in other languages.)

**ninai.constructors.modifiers.Familiar(*args)**

   Used to adjust the containing constructor to reflect a familiar
   inflection (the level indicated by ‘tum’ in Hindustani):

   ::

      >>> Identification(Listener(Familiar()), Possession(Speaker(), Concept(Q(17297777)))) # You are my friend.

   (See also general notes regarding Honorific.)

**ninai.constructors.modifiers.FarDistal(*args)**

   Used to specify that the contained constructor is away from the
   speaker and the listener and somewhat far (much like ‘dot’ in
   Northern Sami):

   ::

      >>> Attribution(FarDistal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

   (See also general notes regarding Demonstrative.)

**ninai.constructors.modifiers.Formal(*args)**

   Used to adjust the containing constructor to reflect a formal
   inflection (the level indicated by ‘aap’ in Hindustani):

   ::

      >>> Identification(Listener(Formal()), Possession(Speaker(), Concept(Q(17297777)))) # You are my friend.

   (See also general notes regarding Honorific.)

**ninai.constructors.modifiers.Honorific(*args)**

   Base class for honorific specifiers.

**ninai.constructors.modifiers.Informal(*args)**

   Used to adjust the containing constructor to reflect an informal
   inflection (the level indicated by ‘tu’ in Hindustani):

   ::

      >>> Identification(Listener(Informal()), Possession(Speaker(), Concept(Q(17297777)))) # Thou art my friend.

   (See also general notes regarding Honorific.)

**ninai.constructors.modifiers.Instance(*args)**

   Used to define a particular instance of a concept as an argument of
   a constructor, rather than the concept by itself:

   ::

      >>> Identification(Concept(Q(319)), Concept(Q(634))) # Jupiter is planet (the concept).
      >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.

   There may be instances, even in languages where definiteness is not
   marked, where omitting this constructor may not make a difference,
   but omitting it might yield undesirable or inconsistent results.

**ninai.constructors.modifiers.MedialDemonstrative(*args)**

   Used to specify that the contained constructor is closer to the
   listener (much like ‘sono’ in Japanese):

   ::

      >>> Attribution(MedialDemonstrative(Concept(Q(10978))), Concept(Q(24245823))) # That grape is small.

   (See also general notes regarding Demonstrative.)

**ninai.constructors.modifiers.Mesiodistal(*args)**

   Used to specify that the contained constructor is away from the
   speaker and the listener but still near (much like ‘duot’ in
   Northern Sami):

   ::

      >>> Attribution(Mesiodistal(Concept(Q(10978))), Concept(Q(24245823))) # That grape over there is small.

   (See also general notes regarding Demonstrative.)

**ninai.constructors.modifiers.Negation(*args)**

   Used to negate different types of constructors:

   ::

      >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.
      >>> Negation(Identification(Concept(Q(319)), Instance(Concept(Q(634))))) # Jupiter is not a planet.

   Essentially an equivalent to the logical ‘not’ operator.

**ninai.constructors.modifiers.Paucal(*args)**

   Paucalizes(?) the constructor it modifies:

   ::

      >>> Possession(Speaker(), Paucal(Concept(Q(133105)))) # I have a few legs.

   (This and other constructors without renderers are subject to
   abrupt change.)

**ninai.constructors.modifiers.Plural(*args)**

   Pluralizes the constructor it modifies:

   ::

      >>> Possession(Speaker(), Plural(Concept(Q(133105)))) # I have legs.

   (This and other constructors without renderers are subject to
   abrupt change.)

**ninai.constructors.modifiers.Proximal(*args)**

   Used to specify that the contained constructor is closer to the
   speaker (much like ‘kono’ in Japanese):

   ::

      >>> Attribution(Proximal(Concept(Q(10978))), Concept(Q(24245823))) # This grape is small.

   (See also general notes regarding Demonstrative.)

**ninai.constructors.modifiers.Reason(*args)**

   Used to indicate that the contained constructor is a cause of the
   statement that contains this modifier:

   ::

      >>> Existence(Listener(), Reason(Possession(Listener(), Concept(Q(9165))))) # You exist because you have a soul.

   The handling of extra arguments beyond the first is yet to be
   determined.

**ninai.constructors.modifiers.Singular(*args)**

   Pluralizes the constructor it modifies:

   ::

      >>> Possession(Speaker(), Plural(Concept(Q(133105)))) # I have legs.

   (This and other constructors without renderers are subject to
   abrupt change.)

**ninai.constructors.modifiers.Superlative(*args)**

   Used to specify that an attribute is the most pronounced:

   ::

      >>> Attribution(Concept(Q(319)), Superlative(Concept(Q(59863338)))) # Jupiter is the largest planet.

   As with other constructors, extra arguments may be added to clarify
   the superlative nature of the attribute.


ninai.constructors.nominals module
==================================

**ninai.constructors.nominals.Imperson(*args)**

   Used as an impersonal pronoun in situations that require it:

   >>> Attribution(Imperson(), Concept(Q(59863338))) # One is large.

   (This will likely be adjusted when renderers for it are created.)

**ninai.constructors.nominals.Listener(*args)**

   Refers to a listener:

   ::

      >>> Attribution(Listener(), Concept(Q(59863338))) # You are large.

   May be pluralized, as Listener(Plural()), or formalized, as
   Listener(Formal()), or both, as Listener(Plural(), Formal()).

**ninai.constructors.nominals.Speaker(*args)**

   Refers to the speaker:

   >>> Attribution(Speaker(), Concept(Q(59863338))) # I am large.

   Unlike Listener, this may not be pluralized; instead, see the
   nominal constructors below this one.

**ninai.constructors.nominals.SpeakerListener(*args)**

   Refers to the speaker and the listener (but not others):

   >>> Attribution(SpeakerListener(), Concept(Q(59863338))) # We (you and I) are large.

**ninai.constructors.nominals.SpeakerListenerOthers(*args)**

   Refers to the speaker, the listener, and others:

   >>> Attribution(SpeakerListenerOthers(), Concept(Q(59863338))) # We (you and I and others) are large.

**ninai.constructors.nominals.SpeakerOthers(*args)**

   Refers to the speaker and others (but not the listener):

   >>> Attribution(SpeakerOthers(), Concept(Q(59863338))) # We (but not you) are large.


ninai.constructors.nonverbal_predicates module
==============================================

**ninai.constructors.nonverbal_predicates.Attribution(*args)**

   Assigns an attribute to an object:

   ::

      >>> Attribution(Concept(Q(319)), Concept(Q(59863338))

   Independently the above may represent “Jupiter is large”, a
   complete statement. As an argument elsewhere it may represent
   “large Jupiter”, a modified concept.

**ninai.constructors.nonverbal_predicates.Benefaction(*args)**

   Indicates that something is for the benefit of something else:

   ::

      >>> Benefaction(Concept(Q(319)), Instance(Concept(Q(58968)), Plural()))

   Independently it may represent “Jupiter is for intellectuals”, a
   complete statement. As an argument elsewhere it may represent
   “Jupiter for intellectuals”, a modified concept.

**ninai.constructors.nonverbal_predicates.Existence(*args)**

   Indicates that something exists:

   ::

      >>> Existence(Instance(Concept(Q(634)))) # A planet exists; there is a planet.

   At the moment considered only as a top-level constructor.

**ninai.constructors.nonverbal_predicates.Identification(*args)**

   Equates subject and object:

   ::

      >>> Identification(Concept(Q(319)), Instance(Concept(Q(634)))) # Jupiter is a planet.

   At the moment considered only as a top-level constructor.

**ninai.constructors.nonverbal_predicates.Possession(*args)**

   Indicates that something owns something else:

   ::

      >>> Possession(Concept(Q(319)), Instance(Concept(Q(179792))))

   Independently it may represent “Jupiter has a ring”, a complete
   statement. As an argument elsewhere it may represent “Jupiter’s
   ring”, a modified concept.


ninai.constructors.tenses module
================================

Holds constructors for different tenses.

**ninai.constructors.tenses.Crastinal(*args)**

   Used for events occurring on the day following the current
   reference time:

   ::

      >>> Possession(Speaker(), Instance(Concept(Q(10817602))), Crastinal()) # I will have a pineapple tomorrow.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.FutureTense(*args)**

   Used for events occurring at any time after the current reference
   time:

   ::

      >>> Possession(Speaker(), Instance(Concept(Q(10817602))), FutureTense()) # I will have a pineapple.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.GeneralTemporal(*args)**

   Base class for modifiers specifying the generic times of
   constructors (akin to providing a tense).

**ninai.constructors.tenses.Hesternal(*args)**

   Used for events occurring on the day prior to the current reference
   time:

   ::

      >>> Possession(Speaker(), Instance(Concept(Q(89))), Hesternal()) # I had an apple yesterday.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.Hodiernal(*args)**

   Used for events occurring on the same day as the current reference
   time:

   ::

      >>> Possession(Speaker(), Instance(Concept(Q(165447))), Hodiernal()) # I have a pen today.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.PastTense(*args)**

   Used for events occurring at any time before the current reference
   time:

   ::

      >>> Possession(Speaker(), Instance(Concept(Q(89))), PastTense()) # I had an apple.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.PresentTense(*args)**

   Used for events occurring at or around the current reference time:

   ::

      >>> Possession(Speaker(), Instance(Concept(Q(165447))), PresentTense()) # I have a pen.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.RecentFuture(*args)**

   Used for events occurring at a time close in the future to the
   current reference time:

   >>> Possession(Speaker(), Instance(Concept(Q(10817602))), RecentFuture()) # I will have a pineapple in a short time from now.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.RecentPast(*args)**

   Used for events occurring at a time close in the past to the
   current reference time:

   >>> Possession(Speaker(), Instance(Concept(Q(89))), RecentPast()) # I had an apple not long ago.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.RemoteFuture(*args)**

   Used for events occurring at a time far removed in the future from
   the current reference time:

   >>> Possession(Speaker(), Instance(Concept(Q(10817602))), RemoteFuture()) # I will have a pineapple a long time from now.

   (See also general notes regarding GeneralTemporal.)

**ninai.constructors.tenses.RemotePast(*args)**

   Used for events occurring at a time far removed in the past from
   the current reference time:

   >>> Possession(Speaker(), Instance(Concept(Q(89))), RemotePast()) # I had an apple a long time ago.

   (See also general notes regarding GeneralTemporal.)


Module contents
===============

Holds all constructors.
